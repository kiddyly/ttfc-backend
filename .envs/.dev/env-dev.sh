export DOCKER_NETWORK='dev_network'

export API_HOST='api.ttfc.dev.gkcsoftware.com'
export PHPPGADMIN_HOST='phppgadmin.ttfc.dev.gkcsoftware.com'
export VIRTUAL_NETWORK='nginx-proxy'
export VIRTUAL_API_PORT='5000'
export VIRTUAL_PHPPGADMIN_PORT='80'
export LETSENCRYPT_API_HOST="$API_HOST"
export LETSENCRYPT_PHPPGADMIN_HOST="$PHPPGADMIN_HOST"
export LETSENCRYPT_EMAIL='devops@giakiemcoder.com'

export APP_NAME='ttfc_dev'

# export TTFC_TRAEFIK_PATH="/home/cicduser/data/${APP_NAME}/traefik"
export TTFC_DJANGO_MEDIA_PATH="/home/cicduser/data/${APP_NAME}/django_media"
export TTFC_DJANGO_GUNICORN_PATH="/home/cicduser/data/${APP_NAME}/django_gunicorn"
export TTFC_POSTGRES_DATA_PATH="/home/cicduser/data/${APP_NAME}/postgres_data"
export TTFC_POSTGRES_DATA_BACKUPS_PATH="/home/cicduser/data/${APP_NAME}/postgres_data_backups"

