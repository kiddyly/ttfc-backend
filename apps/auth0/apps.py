from django.apps import AppConfig


class Auth0Config(AppConfig):
    name = "apps.auth0"
    verbose_name = "Auth0 application"
