from django.contrib.auth import get_user_model
from django.contrib.auth.backends import RemoteUserBackend

UserModel = get_user_model()


class Auth0UserBackend(RemoteUserBackend):
    create_unknown_user = True

    def authenticate(self, request, **kwargs):
        """
        The username passed as ``remote_user`` is considered trusted. Return
        the ``User`` object with the given username. Create a new ``User``
        object if ``create_unknown_user`` is ``True``.
        Return None if ``create_unknown_user`` is ``False`` and a ``User``
        object with the given username is not found in the database.
        """
        if not kwargs.get("remote_user"):
            return
        user = None
        username = self.clean_username(kwargs.get("remote_user"))

        if self.create_unknown_user:
            user, created = UserModel._default_manager.get_or_create(
                email=username,
                defaults={
                    "auth0_id": kwargs.get("auth0_id"),
                    "roles": kwargs.get("roles"),
                    "picture": kwargs.get("picture"),
                    "timezone": kwargs.get("timezone"),
                    "source_id": kwargs.get("source_id")
                },
            )
            if created:
                user = self.configure_user(request, user)
            else:
                # Update user (role, picture) if needed
                need_update = False
                if user.roles != kwargs.get("roles"):
                    user.roles = kwargs.get("roles")
                    need_update = True
                if user.timezone != kwargs.get("timezone"):
                    user.timezone = kwargs.get("timezone")
                    need_update = True

                if need_update:
                    user.save()
        else:
            try:
                user = UserModel._default_manager.get_by_natural_key(username)
            except UserModel.DoesNotExist:
                pass
        return user if self.user_can_authenticate(user) else None
