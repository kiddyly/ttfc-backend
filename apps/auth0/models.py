from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField

from .managers import Auth0UserManager


class Auth0User(AbstractUser):
    username = None
    email = models.EmailField("email address", unique=True)
    auth0_id = models.CharField(max_length=128, unique=True, primary_key=True)
    picture = models.CharField(max_length=250)
    roles = ArrayField(models.CharField(max_length=50, blank=True), size=8,)
    timezone = models.CharField(max_length=50, default="Asia/Ho_Chi_Minh")
    source_id = models.CharField(max_length=50, default="")

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = Auth0UserManager()

    def __str__(self):
        return self.email
