from functools import wraps
import jwt

from django.http import JsonResponse
from rest_framework import permissions

from .utils import get_token_auth_header


def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """

    def require_scope(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            token = get_token_auth_header(args[0])
            decoded = jwt.decode(token, verify=False)
            if decoded.get("permissions"):
                for permission in decoded.get("permissions"):
                    if permission == required_scope:
                        return f(*args, **kwargs)
            response = JsonResponse(
                {"message": "You don't have access to this resource"}
            )
            response.status_code = 403
            return response

        return decorated

    return require_scope


def RequiredPermissions(required_permissions):
    """Create permission class to check permission based on the required permissions input

    Args:
        required_permissions (:obj:`list` of :obj:`str`): List of required permissions

    Returns:
        class: Derive of BasePermission, to check user permission when access to the views
    """

    if not required_permissions or not (
        isinstance(required_permissions, str) or isinstance(required_permissions, list)
    ):
        raise Exception(
            "You must provide required permissions to be able to check the permissions"
        )

    if isinstance(required_permissions, str):
        required_permissions = [required_permissions]

    class class_required_permissions(permissions.BasePermission):
        """Set to APIView to restrict the access based on the user's permissionns"""

        message = "You don't have perrmission to perform this action. Please contact admin if you need help"

        def has_permission(self, request, view):
            decoded = jwt.decode(request.auth, verify=False)
            permissions = decoded.get("permissions", [])

            for required_permission in required_permissions:
                if required_permission not in permissions:
                    return False

            return True

    return class_required_permissions


def RequiredRoles(required_roles):
    """Create permission class to check roles based on the required roles input

    Args:
        required_roles (:obj:`list` of :obj:`str`): List of required roles

    Returns:
        class: Derive of BasePermission, to check user role when access to the views
    """

    if not required_roles or not (
        isinstance(required_roles, str) or isinstance(required_roles, list)
    ):
        raise Exception("You must provide required role to be able to check the roles")

    if isinstance(required_roles, str):
        required_roles = [required_roles]

    class class_required_roles(permissions.BasePermission):
        """Set to APIView to restrict the access based on the user's permissionns"""

        message = "You don't have perrmission to perform this action. Please contact admin if you need help"

        def has_permission(self, request, view):
            decoded = jwt.decode(request.auth, verify=False)
            roles = decoded.get("https://portalfv.com/roles", [])

            for required_role in required_roles:
                if required_role not in roles:
                    return False

            return True

    return class_required_roles


def RequiredRolesOrReadOnly(required_roles):
    """Create permission class to check roles based on the required roles input

    Args:
        required_roles (:obj:`list` of :obj:`str`): List of required roles

    Returns:
        class: Derive of BasePermission, to check user role when access to the views
    """

    if not required_roles or not (
        isinstance(required_roles, str) or isinstance(required_roles, list)
    ):
        raise Exception("You must provide required role to be able to check the roles")

    if isinstance(required_roles, str):
        required_roles = [required_roles]

    class class_required_roles(permissions.BasePermission):
        """Set to APIView to restrict the access based on the user's permissionns"""

        message = "You don't have perrmission to perform this action. Please contact admin if you need help"

        def has_permission(self, request, view):
            if request.method in permissions.SAFE_METHODS:
                return True

            decoded = jwt.decode(request.auth, verify=False)
            roles = decoded.get("https://portalfv.com/roles", [])

            for required_role in required_roles:
                if required_role not in roles:
                    return False

            return True

    return class_required_roles
