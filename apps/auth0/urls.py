from django.urls import path

from . import views

urlpatterns = [
    path("staffs/", views.get_staffs),
    path("staffs/<id>/", views.get_staff),
]
