import json
import jwt
import requests

from django.contrib.auth import authenticate
from django.conf import settings
from django.core.cache import cache

JWKS_CACHE_KEY = "AUTH0_JWKS"


def jwt_get_username_from_payload_handler(payload):
    username = payload.get("https://portalfv.com/email")
    metadata = payload.get("https://portalfv.com/user_metadata")
    source_id = metadata["ProductInfo"]["MoneyTap"]["SourceId"]
    user_data = {
        "auth0_id": payload.get("sub").replace("|", "."),
        "remote_user": username,
        "roles": payload.get("https://portalfv.com/roles"),
        "picture": payload.get("https://portalfv.com/picture"),
        "timezone": payload.get("https://portalfv.com/timezone"),
        "source_id": source_id
    }
    authenticate(**user_data)
    return username


def jwt_decode_token(token):
    header = jwt.get_unverified_header(token)

    jwks = cache.get(JWKS_CACHE_KEY)
    if not jwks:
        jwks = requests.get(
            "https://{}/.well-known/jwks.json".format(settings.AUTH0["DOMAIN"])
        ).json()
        cache.set(JWKS_CACHE_KEY, jwks, 600)

    public_key = None
    for jwk in jwks["keys"]:
        if jwk["kid"] == header["kid"]:
            public_key = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(jwk))

    if public_key is None:
        raise Exception("Public key not found.")

    issuer = "https://{}/".format(settings.AUTH0["DOMAIN"])
    return jwt.decode(
        token,
        public_key,
        audience=settings.AUTH0["AUDIENCE"],
        issuer=issuer,
        algorithms=["RS256"],
    )


def get_token_auth_header(request):
    """Obtains the Access Token from the Authorization Header
    """
    auth = request.META.get("HTTP_AUTHORIZATION", None)
    parts = auth.split()
    token = parts[1]

    return token
