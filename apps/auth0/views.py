from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth import get_user_model

from .permissions import requires_scope
from .serializers import UserSerializer

User = get_user_model()


@api_view(["GET"])
@requires_scope("read:staffs")
def get_staffs(request):
    try:
        users = User.objects.all().filter(
            roles__overlap=["staff"]
        ).order_by('email')
        serializers = UserSerializer(users, many=True)
        return Response(serializers.data)
    except Exception:
        return Response(
            "Unable to get staffs", status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@api_view(["GET"])
@requires_scope("read:staff_detail")
def get_staff(request, id):
    try:
        user = User.objects.get(auth0_id=id)
        serializers = UserSerializer(user)
        return Response(serializers.data)
    except Exception:
        return Response(
            "Unable to get staff detail", status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
