from django.apps import AppConfig


class PortalConfig(AppConfig):
    name = "apps.portal"
    verbose_name = "Portal application"

    def ready(self):
        import apps.portal.signals.ticket  # noqa
        import apps.portal.signals.activity  # noqa
