from django.db import models
from enum import Enum


class FieldRole(Enum):
    PRIMARY_KEY = "primary"
    FOREIGN_KEY = "foreign"
    NORMAL = "normal"


class Flows(models.TextChoices):
    LEAD_GEN = "lead_gen", "Lead Generating"
    PUBLISHED = "applying", "Applying"
