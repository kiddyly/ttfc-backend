from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model

from apps.portal.models.activity import Activity, CallStatus
from apps.portal.enums import Flows

User = get_user_model()


class ActivityFilter(filters.FilterSet):
    doer = filters.ModelChoiceFilter("doer", queryset=User.objects.all())
    flow = filters.MultipleChoiceFilter(choices=Flows.choices)
    customer = filters.ModelChoiceFilter("customer", queryset=User.objects.all())
    status = filters.MultipleChoiceFilter(choices=Activity.STATUS_CHOICES)
    name = filters.CharFilter(lookup_expr="icontains")
    activity_status = filters.CharFilter(lookup_expr="iexact")

    class Meta:
        model = Activity
        fields = "__all__"


class CallStatusFilter(filters.FilterSet):
    is_active = filters.BooleanFilter()

    class Meta:
        model = CallStatus
        fields = ["is_active"]
