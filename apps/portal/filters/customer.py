from django_filters import rest_framework as filters
from ..models.customer import Customer, USER_DATA_HEADER_COLS


class CustomerFilter(filters.FilterSet):
    stage = filters.BaseInFilter()
    scheme = filters.BaseInFilter()
    state = filters.BaseInFilter()
    status_date = filters.DateTimeFromToRangeFilter()
    status = filters.MultipleChoiceFilter(choices=Customer.STATUS_CHOICES)

    class Meta:
        model = Customer
        fields = [item["field"] for item in USER_DATA_HEADER_COLS.values()]
