from django.db.models import Q
from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model

from apps.portal.models.lead import LeadActivity, Lead, LeadStatus

User = get_user_model()


class LeadActivityFilter(filters.FilterSet):
    doer = filters.ModelChoiceFilter("doer", queryset=User.objects.all())
    status = filters.MultipleChoiceFilter(choices=LeadActivity.STATUS_CHOICES)
    notes = filters.CharFilter(lookup_expr="icontains")
    name = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = LeadActivity
        fields = "__all__"


class LeadFilter(filters.FilterSet):
    no_assignee = filters.BooleanFilter("assignee", lookup_expr="isnull")
    updated_at = filters.IsoDateTimeFromToRangeFilter()
    assigned_at = filters.IsoDateTimeFromToRangeFilter()
    customer = filters.CharFilter(method="filter_search")

    def filter_search(self, queryset, name, value):
        """Search Lead by customer id_number, name or phone number

        Args:
            queryset (QuerySet): The queryset
            name (str): The name of field
            value (str): The value of searching

        Returns:
            QuerySet: The result queryset
        """
        return queryset.filter(
            Q(customer__id_number__icontains=value)
            | Q(customer__name__unaccent__icontains=value)
            | Q(customer__phone__icontains=value)
        )

    class Meta:
        model = Lead
        fields = [
            "customer", "product", "created_by",
            "approved_by", "assignee", "ttfc_status",
            "created_from",
        ]


class LeadStatusFilter(filters.FilterSet):
    is_active = filters.BooleanFilter()
    status = filters.MultipleChoiceFilter(choices=LeadStatus.LEAD_STATUS_CHOICES)

    class Meta:
        model = LeadStatus
        fields = ["is_active", "status"]
