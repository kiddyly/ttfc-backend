from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model

from apps.portal.models.moneytab import MoneyTabDistrict
from apps.portal.models.moneytab import MoneyTabWard


User = get_user_model()


class MoneyTabDistrictFilter(filters.FilterSet):
    class Meta:
        model = MoneyTabDistrict
        fields = "__all__"


class MoneyTabWardFilter(filters.FilterSet):
    class Meta:
        model = MoneyTabWard
        fields = "__all__"
