from django.db import models
from django.contrib.auth import get_user_model

from apps.portal.enums import Flows
from .base import TimeCapturedMixin
from apps.portal.models.customer import Customer

User = get_user_model()


class CallStatus(models.Model):
    """Model definition for CallStatus."""

    AGREED = "AGREED"
    DECLINED = "DECLINED"
    ONHOLD = "ONHOLD"

    CALL_STATUS_CHOICES = (
        (AGREED, "Agreed"),
        (DECLINED, "Declined"),
        (ONHOLD, "OnHold"),
    )

    status = models.CharField(max_length=20, choices=CALL_STATUS_CHOICES)
    additional_information = models.CharField(max_length=255, default="")
    is_active = models.BooleanField(default=True)
    allow_reuse = models.BooleanField(default=False)

    class Meta:
        """Meta definition for CallStatus."""

        verbose_name = "CallStatus"
        verbose_name_plural = "CallStatuses"
        unique_together = ("status", "additional_information")

    def __str__(self):
        """Unicode representation of CallStatus."""
        return "{} - {}".format(self.status, self.additional_information)


class Activity(TimeCapturedMixin):
    """Model definition for Activity."""

    PEDNING = "pending"
    INPROGRESS = "inprogress"
    DONE = "done"

    STATUS_CHOICES = (
        (INPROGRESS, "In Progress"),
        (PEDNING, "Pending"),
        (DONE, "Done"),
    )

    doer = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    flow = models.CharField(max_length=30, choices=Flows.choices)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    notes = models.CharField(max_length=250, blank=True, default="")
    name = models.CharField(max_length=250)

    # This will be set with the value from ActivityStatus
    call_status = models.ForeignKey(CallStatus, on_delete=models.SET_NULL, null=True)

    class Meta:
        """Meta definition for Activity."""

        verbose_name = "Activity"
        verbose_name_plural = "Activities"

    def __str__(self):
        """Unicode representation of Activity."""
        return "{} - {} - {}".format(self.name, self.status, self.flow)
