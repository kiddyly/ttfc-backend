import datetime
from django.db import models

from apps.portal.enums import FieldRole
from .base import TimeCapturedMixin


# Create your models here.
def parse_date(date_str):
    if date_str:
        try:
            return datetime.datetime.strptime(date_str, "%d/%m/%Y").date()
        except Exception:
            return None
    return None


def parse_string(input_str):
    return str(input_str) if input_str else ""


USER_DATA_HEADER_COLS = {
    "ID": {
        "field": "auto_increment_id",
        "filter": False,
        "role": FieldRole.PRIMARY_KEY,
    },
    "CertNumber": {
        "field": "id_number",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.PRIMARY_KEY,
    },
    "Cert issue date": {
        "field": "issue_date",
        "formater": parse_date,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Cert issue place": {
        "field": "issue_place",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Name": {
        "field": "name",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Phone Number": {
        "field": "phone",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Gender": {
        "field": "gender",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Age": {"field": "age", "formater": int, "filter": False, "role": FieldRole.NORMAL},
    "Date of Birth": {
        "field": "date_of_birth",
        "formater": parse_date,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Ward": {
        "field": "ward",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Stage": {
        "field": "stage",
        "formater": parse_string,
        "filter": True,
        "role": FieldRole.NORMAL,
    },
    "Scheme": {
        "field": "scheme",
        "formater": parse_string,
        "filter": True,
        "role": FieldRole.NORMAL,
    },
    "State": {
        "field": "state",
        "formater": parse_string,
        "filter": True,
        "role": FieldRole.NORMAL,
    },
    "Company": {
        "field": "company",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Status Date": {
        "field": "status_date",
        "formater": parse_date,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "NotePad": {
        "field": "notepad",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Street": {
        "field": "street",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "District/Town": {
        "field": "district",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Income": {
        "field": "income",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Loan Amount": {
        "field": "loan_amount",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Loan Time": {
        "field": "loan_period",
        "formater": parse_string,
        "filter": False,
        "role": FieldRole.NORMAL,
    },
    "Status": {"field": "status", "filter": True, "role": FieldRole.NORMAL},
}


class Customer(TimeCapturedMixin):
    """Model definition for Customer."""

    DELETED = "deleted"
    UNAVAILABLE = "unavailable"
    MANUAL_POOL = "manual_pool"

    STATUS_CHOICES = (
        (DELETED, "Deleted"),
        (UNAVAILABLE, "Unavailable"),
        (MANUAL_POOL, "Manual Pool"),
    )

    auto_increment_id = models.AutoField(primary_key=True)
    id_number = models.CharField(max_length=20, unique=True)
    issue_date = models.DateField(blank=True, null=True)
    issue_place = models.CharField(max_length=100, blank=True)
    name = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=15, blank=True)
    gender = models.CharField(max_length=6, blank=True)
    age = models.CharField(max_length=3, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)

    ward = models.CharField(max_length=100, blank=True)
    stage = models.CharField(max_length=200, blank=True)
    scheme = models.CharField(max_length=200, blank=True)
    state = models.CharField(max_length=200, blank=True)
    company = models.CharField(max_length=200, blank=True)
    status_date = models.DateField(blank=True, null=True)
    notepad = models.TextField(blank=True)

    street = models.CharField(max_length=100, blank=True)
    district = models.CharField(max_length=100, blank=True)
    income = models.CharField(max_length=100, blank=True)
    loan_amount = models.CharField(max_length=100, blank=True)
    loan_period = models.CharField(max_length=100, blank=True)
    status = models.CharField(
        max_length=50, choices=STATUS_CHOICES, default=UNAVAILABLE
    )
    latest_call_activity = models.OneToOneField(
        "Activity",
        on_delete=models.SET_NULL,
        related_name="latest_of_customer",
        null=True,
    )

    # extra fields
    email = models.EmailField(blank=True, null=True, default=None)
    company_type = models.CharField(max_length=50, blank=True, null=True, default=None)
    job_type = models.CharField(max_length=50, blank=True, null=True, default=None)
    employment_type = models.CharField(max_length=50, blank=True, null=True, default=None)
    residence_type = models.CharField(max_length=50, blank=True, null=True, default=None)
    marital_status = models.CharField(max_length=50, blank=True, null=True, default=None)
    job_title_type = models.CharField(max_length=255, blank=True, null=True, default=None)
    occupation_type = models.CharField(max_length=255, blank=True, null=True, default=None)
    current_work_experience_in_months = models.CharField(max_length=5, default='1')
    total_work_experience_in_months = models.CharField(max_length=5, default='1')
    current_city_duration_in_months = models.CharField(max_length=5, default='1')
    current_home_address_duration_in_months = models.CharField(max_length=5, default='1')

    def __str__(self):
        return "{} - {}".format(self.id_number, self.name)
