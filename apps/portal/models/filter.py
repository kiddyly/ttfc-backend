from django.db import models


class TableFilter(models.Model):
    table_id = models.CharField(max_length=50)
    filter_field_name = models.CharField(max_length=20)
    filter_field_value = models.CharField(max_length=200)

    class Meta:
        unique_together = ("table_id", "filter_field_name", "filter_field_value")
