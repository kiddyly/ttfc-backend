from django.contrib.postgres.fields.jsonb import JSONField
from django.db import models
from django.contrib.auth import get_user_model

from .customer import Customer
from .product import Product
from .base import TimeCapturedMixin
from .activity import Activity

User = get_user_model()


class LeadStatus(models.Model):
    """Model definition for LeadStatus."""

    DONE = "Done"
    SUBMITTED = "Submitted"
    PENDING = "Pending"
    DECLINED = "Declined"

    LEAD_STATUS_CHOICES = (
        (SUBMITTED, "Submitted"),
        (PENDING, "Pending"),
        (DECLINED, "Declined"),
        (DONE, "Done"),
    )

    status = models.CharField(max_length=20, choices=LEAD_STATUS_CHOICES)
    additional_information = models.CharField(max_length=255, default="")
    is_active = models.BooleanField(default=True)

    class Meta:
        """Meta definition for LeadStatus."""

        verbose_name = "LeadStatus"
        verbose_name_plural = "LeadStatuses"
        unique_together = ("status", "additional_information")

    def __str__(self):
        """Unicode representation of LeadStatus."""
        return "{} - {}".format(self.status, self.additional_information)


class Lead(TimeCapturedMixin):
    """Model definition for Lead."""

    PRODUCT_MONEYTAB_KEY = "moneytab"

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    created_by = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="creator", null=True
    )
    approved_by = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="approver", null=True
    )
    assignee = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="assignee", null=True
    )
    assigned_at = models.DateTimeField(null=True)
    ttfc_status = models.ForeignKey(LeadStatus, on_delete=models.SET_NULL, null=True)
    created_from = models.ForeignKey(Activity, on_delete=models.SET_NULL, null=True)

    # fields for moneytab integration
    product_metadata = JSONField(null=True, default=None)
    # product_status = models.CharField(
    #     max_length=100, blank=True, null=True, default=None, db_index=True
    # )
    # product_status_at = models.DateTimeField(null=True, default=None)

    class Meta:
        """Meta definition for Lead."""

        verbose_name = "Lead"
        verbose_name_plural = "Leads"

    def get_moneytab_product_metadata(self):
        if self.product_metadata is None:
            return dict(
                customer_id=None,
                customer_status=None,
                updated_at=None
            )

        return self.product_metadata[self.PRODUCT_MONEYTAB_KEY]

    def get_moneytab_customer_id(self):
        if self.product_metadata is None:
            return None

        return self.product_metadata[self.PRODUCT_MONEYTAB_KEY]["customer_id"]

    def get_moneytab_customter_status(self):
        if self.product_metadata is None:
            return None

        return self.product_metadata[self.PRODUCT_MONEYTAB_KEY]["customer_status"]

    def set_moneytab_product_metadata(
        self, customer_id=None, customer_status=None,
        current_status=None, updated_at=None, source=None
    ):
        if self.product_metadata is None:
            self.product_metadata = dict()
            self.product_metadata[self.PRODUCT_MONEYTAB_KEY] = dict(
                customer_id=customer_id,
                customer_status=customer_status,
                current_status=current_status,
                updated_at=updated_at,
                source=source
            )
        else:
            self.product_metadata[self.PRODUCT_MONEYTAB_KEY] = dict(
                customer_id=customer_id,
                customer_status=customer_status,
                current_status=current_status,
                updated_at=updated_at,
                source=source
            )

    def __str__(self):
        """Unicode representation of Lead."""
        return "{} - {}".format(self.customer, self.product)


class LeadActivity(TimeCapturedMixin):
    """Model definition for Lead Activity."""

    PEDNING = "pending"
    INPROGRESS = "inprogress"
    DONE = "done"

    STATUS_CHOICES = (
        (INPROGRESS, "In Progress"),
        (PEDNING, "Pending"),
        (DONE, "Done"),
    )

    doer = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    notes = models.CharField(max_length=250, blank=True, default="")
    name = models.CharField(max_length=250)
    order = models.DecimalField(max_digits=20, decimal_places=0)

    class Meta:
        """Meta definition for Lead Activity."""

        verbose_name = "Lead Activity"
        verbose_name_plural = "Lead Activities"

    def __str__(self):
        """Unicode representation of Lead Activity."""
        return "{} - {}".format(self.name, self.status)
