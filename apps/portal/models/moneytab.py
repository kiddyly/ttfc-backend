from django.db import models


class MoneyTabProvince(models.Model):
    """Model definition for MoneyTab's Province."""
    name = models.CharField(max_length=255, db_index=True)
    code = models.PositiveSmallIntegerField(db_index=True)

    class Meta:
        """Meta definition for MoneyTabProvince."""

        verbose_name = "MoneyTabProvince"
        verbose_name_plural = "MoneyTabProvinces"

    def __str__(self):
        """Unicode representation of MoneyTabProvince."""
        return self.name


class MoneyTabDistrict(models.Model):
    """Model definition for MoneyTab's District."""
    name = models.CharField(max_length=255, db_index=True)
    code = models.PositiveSmallIntegerField(db_index=True)
    province = models.ForeignKey(
        MoneyTabProvince, on_delete=models.CASCADE, related_name="mt_district_provinces"
    )

    class Meta:
        """Meta definition for MoneyTabDistrict."""

        verbose_name = "MoneyTabDistrict"
        verbose_name_plural = "MoneyTabDistricts"

    def __str__(self):
        """Unicode representation of MoneyTabDistrict."""
        return self.name


class MoneyTabWard(models.Model):
    """Model definition for MoneyTab's Ward."""
    name = models.CharField(max_length=255, db_index=True)
    code = models.PositiveSmallIntegerField(db_index=True, null=True)
    district = models.ForeignKey(
        MoneyTabDistrict, on_delete=models.CASCADE, related_name="mt_ward_districts"
    )

    class Meta:
        """Meta definition for MoneyTabDistrict."""

        verbose_name = "MoneyTabWard"
        verbose_name_plural = "MoneyTabWards"

    def __str__(self):
        """Unicode representation of MoneyTabWard."""
        return self.name
