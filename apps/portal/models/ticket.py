from django.db import models
from django.contrib.auth import get_user_model

from .base import TimeCapturedMixin

User = get_user_model()


class Ticket(TimeCapturedMixin):
    """Model definition for Ticket."""

    PENDING = "pending"
    APPROVED = "approved"
    DECLINED = "declined"

    TICKET_STATUS_CHOICES = (
        (PENDING, "Pending"),
        (APPROVED, "Approved"),
        (DECLINED, "Declined"),
    )

    name = models.CharField(max_length=250)
    requester = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=250, default="")
    status = models.CharField(
        max_length=50, choices=TICKET_STATUS_CHOICES, default=APPROVED
    )

    class Meta:
        """Meta definition for Ticket."""

        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"

    def __str__(self):
        """Unicode representation of Ticket."""
        return self.name
