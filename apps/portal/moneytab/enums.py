from enum import Enum


class EnumChoices(Enum):
    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)

    @classmethod
    def choices_reverse(cls):
        return tuple((i.name, i.value) for i in cls)


class CompanyType(EnumChoices):
    OTHER = 'OTHER'


class JobType(EnumChoices):
    SALARIED = 'SALARIED'
    SELF_EMPLOYED_BUSINESS = 'SELF_EMPLOYED_BUSINESS'
    SELF_EMPLOYED_PROFESSIONAL = 'SELF_EMPLOYED_PROFESSIONAL'
    STUDENT = 'STUDENT'
    HOMEMAKER = 'HOMEMAKER'
    RETIRED = 'RETIRED'


class EmploymentType(EnumChoices):
    FULL_TIME = 'FULL_TIME'
    PART_TIME = 'PART_TIME'
    CONTRACT = 'CONTRACT'


class ResidenceType(EnumChoices):
    UNKNOWN = 'UNKNOWN'


class Gender(EnumChoices):
    MALE = 'MALE'
    FEMALE = 'FEMALE'


class MaritalStatus(EnumChoices):
    SINGLE = 'Độc thân'
    MARRIED = 'Đã kết hôn'


class JobTitleType(EnumChoices):
    EDUCATION_ACCOUNTING_DIVISION = 'Khối giáo dục – Kế toán'
    EDUCATION_DEAN_ASSOCIATE_DEAN = 'Khối giáo dục – Trưởng khoa & phó khoa'
    EDUCATION_DIVISION_MANAGER_DEPUTY_MANAGER = 'Khối giáo dục –  Trưởng phòng & phó phòng'
    EDUCATION_DIVISION_CENTER_DIRECTOR_DEPUTY_DIRECTOR = 'Khối giáo dục – Giám đốc & phó giám đốc trung tâm'
    SCHOOL_UNION_PRESIDENT_VICE_PRESIDENT = 'Chủ tịch & phó chủ tịch công đoàn trường học'
    SCHOOL_BOARD_PRESIDENT_VICE_PRESIDENT = 'Chủ tịch & phó chủ tịch hội đồng trường học'
    PRINCIPAL_ASSISTANT_PRINCIPAL = 'Hiệu trưởng & hiệu phó'
    OTHER_SERVICE_JOBS = 'Công việc dịch vụ khác'
    DELIVERY_LEADER_CONVEYOR_LEADER = 'Tổ trưởng giao nhận, chuyền trưởng'
    LEADER_OF_LABOR_FORCE_ASSISTANT = 'Tổ trưởng lao công/ tạp vụ'
    DELIVERY_STAFF = 'Nhân viên giao nhận'
    LABORERS_MAIDS = 'Lao công, tạp vụ'
    MULTILEVEL_SALES = 'Bán hàng đa cấp'
    SECURITIES_BROKERAGE = 'Kinh doanh môi giới chứng khoán'
    REALESTATE_BUSINESS = 'Kinh doanh bất động sản'
    INSURANCE_BUSINESS = 'Kinh doanh bảo hiểm'
    CAR_BUSINESS = 'Kinh doanh xe oto'
    RETIREMENT = 'Nghỉ hưu'
    INTERNSHIP_STUDENT = 'Thực tập/sinh viên'
    SELF_EMPLOYED = 'Tự kinh doanh'
    OTHER_INDUSTRY_EXPERTS = 'Chuyên gia ngành khác'
    SECURITY_GUARD_STAFF_LEVEL = 'Nhân viên bảo vệ - Cấp nhân viên'
    SECURITY_GUARD_TEAM_LEADER_DEPUTY_LEADER = 'Nhân viên bảo vệ - Cấp tổ trưởng/tổ phó'
    AGRICULTURE_CAREER_ADMINISTRATIVE = 'Khối NN – Sự nghiệp – HC – Cấp cán bộ/chuyên viên'
    AGRICULTURE_CAREER_HC_HEAD_DEPUTY = 'Khối NN – Sự nghiệp – HC – Cấp trưởng/phó đơn vị'
    OFFICEBLOCK_LEVELMANAGER_TEAMLEADER = 'Khối văn phòng – Cấp CBQL/Trưởng nhóm'
    OFFICEBLOCK_HEADOFDEPARTMENT = 'Khối văn phòng – Trưởng phòng'
    OFFICEBLOCK_CONTROL = 'Khối văn phòng – Kiểm soát'
    OFFICEBLOCK_BOARDOFDIRECTORS = 'Khối văn phòng – Cấp HĐQT/Ban điều hành'
    NURSES = 'Y tá/Điều dưỡng'
    DOCTOR_DENTIST = 'Bác sĩ/Nha sĩ'
    TEACHER_UNDER_3_CRITERIA = 'Giáo viên không thuộc 03 tiêu chí trên'
    TEACHER_MORE_3_YEARS = 'Giáo viên dạy những môn khác trên 3 năm'
    TEACHER_5_SUB_3_TO_10_YEARS = 'Giáo viên 05 môn chính trên 10 năm'
    FIREFIGHTER_SENIOR_LIEUTENANT = 'Cứu hỏa – Cấp tá tướng'
    CUSTOMS_SENIOR_LIEUTENANT = 'Hải quan – Cấp tá tướng'
    POLICE_SENIOR_LIEUTENANT = 'Công an – Cấp tá tướng'
    ARMY_SENIOR_LIEUTENANT = 'Bộ đội – Cấp tá tướng'
    FIREFIGHTER_OFFICER_LIEUTENANT = 'Hải quan – Cấp sỹ/úy'
    POLICE_OFFICER_LIEUTENANT = 'Công an – Cấp sỹ/úy'
    ARMY_OFFICER_LIEUTENANT = 'Bộ đội – Cấp sỹ/úy'
    EMPLOYEE_LEVEL_WORKERS = 'Công nhân cấp nhân viên'
    VICE_LEVEL_WORKER = 'Công nhân cấp tổ phó hoặc tương tự'
    GROUP_LEADER = 'Công nhân cấp tổ trưởng hoặc tương tự'
    OFFICEBLOCK_BOARDOFDIRECTORS_CHIEFACCOUNTANT = 'Khối văn phòng – Ban giám đốc/Kế toán trưởng'
    OFFICEBLOCK_SPECIALIST_LEVEL = 'Khối văn phòng – Cấp nhân viên/Chuyên viên'


class OccupationType(EnumChoices):
    FINANCE_BANKING_INSURANCE = 'Tài chính/Ngân hàng/Bảo hiểm'
    CONSTRUCTION_ARCHITECTURE = 'Xây dựng/Kiến trúc'
    TRAVEL = 'Du lịch'
    EDUCATIONS = 'Giáo dục/Đào tạo'
    MEDICAL_HEALTHCARE = 'Y tế/Chăm sóc sức khỏe'
    COMMUNICATION_MARKETING = 'Truyền thông/Marketing'
    TELECOMMUNICATION = 'Viễn thông'
    RETAIL = 'Bán lẻ'
    CUSTOMER_SERVICE = 'Dịch vụ khách hàng'
    WHOLESALE_IMPORT_EXPORT = 'Bán buôn/Xuất-nhập khẩu'
    IT_TECHNOLOGY = 'IT/Công nghệ'
    CN_ENERGY_ELECTRICITY_WATER_OIL = 'CN năng lượng (điện/nước/dầu..)'
    REAL_ESTATE_INVESTMENT_TRADING = 'Kinh doanh/đầu tư bất động sản'
    SCIENCE_RESEARCH_APPLICATION = 'Khoa học (nghiên cứu, ứng dụng)'
    AGRICULTURE_FORESTRY_FISHERY = 'Nông lâm ngư nghiệp'
    MANUFACTURING_INDUSTRY = 'Công nghiệp sản xuất, chế tạo'
    GOVERNMENT_MINISTRY_DEPARTMENT = 'Chính phủ (bộ/ban/ngành..)'
    TAXI_TRANSPORTATION = 'Taxi/Giao thông/Vận tải'
    POLICE = 'Cảnh sát/công an'
    ARMY = 'Quân đội'
    FOOD_DRINK_RESTAURANT = 'Thực phẩm/Đồ uống/Nhà hàng'
    LEGAL_CONSULTING_SERVICES = 'Tư vấn/Dịch vụ pháp lý'
    HANDICRAFT = 'Thủ công nghiệp'
    RELIGION_HUMANITARIAN = 'Tôn giáo/Nhân đạo'
    ACCOUNTING_AUDIT = 'Kiểm toán/Kế toán'
