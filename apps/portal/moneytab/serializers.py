from django.conf import settings
from rest_framework import serializers

from .enums import CompanyType
from .enums import JobType
from .enums import EmploymentType
from .enums import ResidenceType
from .enums import Gender
from .enums import MaritalStatus
from .enums import JobTitleType
from .enums import OccupationType


class PartnerLeadSerializer(serializers.Serializer):
    """[Partner Lead Serializer to help validate Partner Lead data]
    """

    fullname = serializers.CharField(min_length=1)
    phone = serializers.CharField(min_length=1)
    birth_date = serializers.CharField(min_length=1)
    birth_month = serializers.CharField(min_length=1)
    birth_year = serializers.CharField(min_length=1)

    gender = serializers.ChoiceField(
        choices=Gender.choices()
    )
    national_id = serializers.CharField(min_length=1)
    issue_date = serializers.CharField(min_length=1)
    issue_month = serializers.CharField(min_length=1)
    issue_year = serializers.CharField(min_length=1)

    marital_status = serializers.ChoiceField(
        default=MaritalStatus.SINGLE.value,
        choices=MaritalStatus.choices_reverse()
    )
    source = serializers.CharField(min_length=1, default=settings.MONEYTAB["TTFC_SOURCE"])
    email_id = serializers.EmailField()
    monthly_net_take_home_salary = serializers.CharField(min_length=1)

    company_type = serializers.ChoiceField(
        default=CompanyType.OTHER.value,
        choices=CompanyType.choices()
    )
    job_type = serializers.ChoiceField(
        default=JobType.SALARIED.value,
        choices=JobType.choices()
    )
    company_name = serializers.CharField(min_length=1)
    employment_type = serializers.ChoiceField(
        default=EmploymentType.FULL_TIME.value,
        choices=EmploymentType.choices()
    )
    occupation_type = serializers.ChoiceField(
        # default=OccupationType.TRAVEL.value,
        choices=OccupationType.choices_reverse()
    )

    job_title_type = serializers.ChoiceField(
        # default=JobTitleType.EDUCATION_ACCOUNTING_DIVISION.value,
        choices=JobTitleType.choices_reverse()
    )
    current_work_experience_in_months = serializers.CharField(min_length=1, default='1')
    total_work_experience_in_months = serializers.CharField(min_length=1, default='1')

    home_address_number = serializers.CharField(min_length=1, default='*')
    home_address_street = serializers.CharField(min_length=1, default='*')
    home_address_state = serializers.CharField(min_length=1)
    home_address_city = serializers.CharField(min_length=1)

    current_city = serializers.CharField(min_length=1)
    home_address_ward = serializers.CharField(min_length=1)
    residence_type = serializers.ChoiceField(
        default=ResidenceType.UNKNOWN.value,
        choices=ResidenceType.choices()
    )

    current_city_duration_in_months = serializers.CharField(min_length=1, default='1')
    current_home_address_duration_in_months = serializers.CharField(min_length=1, default='1')

    @property
    def moneytab_serialized_data(self):
        self.is_valid(raise_exception=True)
        return {
            "fullname": self.validated_data["fullname"],
            "phone": self.validated_data["phone"],
            "birth_Date": self.validated_data["birth_date"],
            "birth_Month": self.validated_data["birth_month"],
            "birth_Year": self.validated_data["birth_year"],
            "gender": self.validated_data["gender"],
            "nationalId": self.validated_data["national_id"],
            "issue_Date": self.validated_data["issue_date"],
            "issue_Month": self.validated_data["issue_month"],
            "issue_Year": self.validated_data["issue_year"],
            "maritalStatus": self.validated_data["marital_status"],
            "source": self.validated_data["source"],
            "emailId": self.validated_data["email_id"],
            "monthlyNetTakeHomeSalary": self.validated_data["monthly_net_take_home_salary"],
            "companyType": self.validated_data["company_type"],
            "jobType": self.validated_data["job_type"],
            "companyName": self.validated_data["company_name"],
            "employmentType": self.validated_data["employment_type"],
            "occupationType": self.validated_data["occupation_type"],
            "jobTitleType": self.validated_data["job_title_type"],
            "currentWorkExperienceInMonths": self.validated_data["current_work_experience_in_months"],
            "totalWorkExperienceInMonths": self.validated_data["total_work_experience_in_months"],
            "homeAddressNumber": self.validated_data["home_address_number"],
            "homeAddressStreet": self.validated_data["home_address_street"],
            "homeAddressState": self.validated_data["home_address_state"],
            "homeAddressCity": self.validated_data["home_address_city"],
            "currentCity": self.validated_data["current_city"],
            "homeAddressWard": self.validated_data["home_address_ward"],
            "residenceType": self.validated_data["residence_type"],
            "currentCityDurationInMonths": self.validated_data["current_city_duration_in_months"],
            "currentHomeAddressDurationInMonths": self.validated_data["current_home_address_duration_in_months"],
        }

    def validate_fullname(self, value):
        return value.upper()

    def validate_phone(self, value):
        return value

    def validate_birth_date(self, value):
        try:
            int_value = int(value)
            if int_value <= 0 or int_value > 31:
                raise serializers.ValidationError("Invalid day of month. Required 0 < birth_date <= 31")
        except ValueError:
            raise serializers.ValidationError("Field 'birth_date' required a number as input")
        return value

    def validate_birth_month(self, value):
        try:
            int_value = int(value)
            if int_value <= 0 or int_value > 12:
                raise serializers.ValidationError("Invalid month. Required 0 < birth_month <= 12")
        except ValueError:
            raise serializers.ValidationError("Field 'birth_month' required a number as input")
        return value

    def validate_birth_year(self, value):
        try:
            int_value = int(value)
            if int_value <= 0:
                raise serializers.ValidationError("Invalid year. Required birth_year > 0")
        except ValueError:
            raise serializers.ValidationError("Field 'birth_year' required a number as input")
        return value

    def validate_gender(self, value):
        return value

    def validate_national_id(self, value):
        return value

    def validate_issue_date(self, value):
        try:
            int_value = int(value)
            if int_value <= 0 or int_value > 31:
                raise serializers.ValidationError("Invalid day of month. Required 0 < issue_date <= 31")
        except ValueError:
            raise serializers.ValidationError("Field 'issue_date' required a number as input")
        return value

    def validate_issue_month(self, value):
        try:
            int_value = int(value)
            if int_value <= 0 or int_value > 12:
                raise serializers.ValidationError("Invalid month. Required 0 < validate_issue_month <= 12")
        except ValueError:
            raise serializers.ValidationError("Field 'validate_issue_month' required a number as input")
        return value

    def validate_issue_year(self, value):
        try:
            int_value = int(value)
            if int_value <= 0:
                raise serializers.ValidationError("Invalid year. Required issue_year > 0")
        except ValueError:
            raise serializers.ValidationError("Field 'issue_year' required a number as input")
        return value

    def validate_marital_status(self, value):
        return MaritalStatus[value].value

    def validate_source(self, value):
        return value

    def validate_email_id(self, value):
        return value

    def validate_monthly_net_take_home_salary(self, value):
        try:
            value = value.split(".")[0]
        except Exception:
            pass

        try:
            int(value)
        except ValueError:
            raise serializers.ValidationError("Field 'monthly_net_take_home_salary' required a number as input")

        return value

    def validate_company_type(self, value):
        return value

    def validate_job_type(self, value):
        return value

    def validate_company_name(self, value):
        return value.upper()

    def validate_employment_type(self, value):
        return value

    def validate_occupation_type(self, value):
        return OccupationType[value].value

    def validate_job_title_type(self, value):
        return JobTitleType[value].value

    def validate_current_work_experience_in_months(self, value):
        try:
            int(value)
        except ValueError:
            raise serializers.ValidationError("Field 'current_work_experience_in_months' required a number as input")

        return value

    def validate_total_work_experience_in_months(self, value):
        try:
            int(value)
        except ValueError:
            raise serializers.ValidationError("Field 'total_work_experience_in_months' required a number as input")

        return value

    def validate_home_address_number(self, value):
        return value

    def validate_home_address_street(self, value):
        return value

    def validate_home_address_state(self, value):
        return value

    def validate_home_address_city(self, value):
        return value

    def validate_current_city(self, value):
        return value

    def validate_home_address_ward(self, value):
        return value

    def validate_residence_type(self, value):
        return value

    def validate_current_city_duration_in_months(self, value):
        try:
            int(value)
        except ValueError:
            raise serializers.ValidationError("Field 'current_city_duration_in_months' required a number as input")

        return value

    def validate_current_home_address_duration_in_months(self, value):
        try:
            int(value)
        except ValueError:
            raise serializers.ValidationError(
                "Field 'current_home_address_duration_in_months' required a number as input"
            )

        return value
