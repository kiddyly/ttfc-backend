import base64
import json
import logging
import requests
from datetime import datetime
from django.conf import settings
from django.core.cache import cache

from .serializers import PartnerLeadSerializer


LOGGER = logging.getLogger(__name__)
MONEYTAB_TOKEN_KEY_CACHE = "MONEYTAB_TOKEN"


class BearerAuth(requests.auth.AuthBase):

    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["Authorization"] = "Bearer " + self.token
        return r


class MoneyTabEndPoints:
    ACCESS_TOKEN_API = "{}/oauth/token?grant_type=client_credentials".format(
        settings.MONEYTAB["BASE_URL"]
    )
    PARTNER_LEAD_GENERATION_API = "{}/partner/acqportal/lead".format(
        settings.MONEYTAB["BASE_URL"]
    )
    PARTNER_CUSTOMER_DETAIL_API = "{}/partner/customer/details".format(
        settings.MONEYTAB["BASE_URL"]
    )


class MoneyTabToken:

    def __init__(self):
        self.access_token = None
        self.expires_in = 0
        self.issued_at = MoneyTabToken.get_ts_utcnow()
        self.scope = None
        self.token_type = None
        self.__get_moneytab_token()

    @classmethod
    def get_bearer_token(cls) -> str:
        return cls().access_token

    @classmethod
    def expires_in_seconds(cls) -> int:
        token = cls()
        if MoneyTabToken.is_token_expired(token.expires_in, token.issued_at):
            return 0

        return (token.issued_at + token.expires_in) - cls.get_ts_utcnow()

    @staticmethod
    def get_ts_utcnow() -> float:
        return datetime.utcnow().timestamp()

    @staticmethod
    def get_encoded_base64_from_string(input_string) -> str:
        assert isinstance(input_string, str), (
            "'get_encoded_base64_from_string' is required a string as input"
        )
        ascii_encoded = input_string.encode('ascii')
        return base64.b64encode(ascii_encoded).decode('utf-8')

    @staticmethod
    def get_basic_token(
        mt_client_id=settings.MONEYTAB["CLIENT_ID"],
        mt_client_secret=settings.MONEYTAB["CLIENT_SECRET"]
    ) -> str:
        assert mt_client_id is not None, (
            "MONEYTAB_CLIENT_ID is None"
        )
        assert mt_client_secret is not None, (
            "MONEYTAB_CLIENT_SECRET is None"
        )
        return MoneyTabToken.get_encoded_base64_from_string("{client_id}:{client_secret}".format(
            client_id=mt_client_id,
            client_secret=mt_client_secret
        ))

    @staticmethod
    def request_bearer_token_from_moneytab(
        api_uri=MoneyTabEndPoints.ACCESS_TOKEN_API,
        request_timeout=5,
        mt_client_id=None,
        mt_client_secret=None
    ) -> object:
        if mt_client_id and mt_client_secret:
            basic_token = MoneyTabToken.get_basic_token(mt_client_id, mt_client_secret)
        else:
            basic_token = MoneyTabToken.get_basic_token()

        headers = {
            'Authorization': 'Basic ' + basic_token
        }
        LOGGER.debug("Get Basic Token from MoneyTab with token_api_uri={} and headers={}".format(
            api_uri,
            headers
        ))
        r = requests.post(api_uri, data=None, headers=headers, timeout=request_timeout)
        r.raise_for_status()
        res_json = r.json()

        return res_json

    @staticmethod
    def is_token_expired(token_expires_in, issued_at) -> bool:
        ts_utcnow = MoneyTabToken.get_ts_utcnow()
        diff = ts_utcnow - issued_at
        if diff >= token_expires_in:
            return True

        return False

    def __set_properties(self, access_token, expires_in, issued_at, token_type, scope):
        self.access_token = access_token
        self.expires_in = expires_in
        self.issued_at = issued_at
        self.token_type = token_type
        self.scope = scope

    def __get_moneytab_token(self):
        should_request_new_token = False

        cached_token = cache.get(MONEYTAB_TOKEN_KEY_CACHE, None)
        if cached_token:
            is_expired = MoneyTabToken.is_token_expired(
                cached_token["expires_in"], cached_token["issued_at"]
            )

            if not is_expired:
                self.__set_properties(
                    cached_token["access_token"],
                    cached_token["expires_in"],
                    cached_token["issued_at"],
                    cached_token["token_type"],
                    cached_token["scope"]
                )
            else:
                should_request_new_token = True

        if not cached_token or should_request_new_token:
            res_json = MoneyTabToken.request_bearer_token_from_moneytab()
            issued_at = MoneyTabToken.get_ts_utcnow()
            self.__set_properties(
                res_json["access_token"],
                res_json["expires_in"],
                issued_at,
                res_json["token_type"],
                res_json["scope"]
            )
            cache.set(MONEYTAB_TOKEN_KEY_CACHE, dict(
                access_token=res_json["access_token"],
                expires_in=res_json["expires_in"],
                issued_at=issued_at,
                token_type=res_json["token_type"],
                scope=res_json["scope"],
            ), res_json["expires_in"])

        return self

    def __str__(self):
        return "access_token={} - expires_in={} - issued_at={}".format(
            self.access_token, self.expires_in, self.issued_at
        )


class MoneyTabRequester:

    def __init__(self):
        self.bearer_token = MoneyTabToken.get_bearer_token()
        self.bearer_auth = BearerAuth(self.bearer_token)

    def get(self, url, raise_for_status=True) -> requests.Response:
        response = requests.get(url, auth=self.bearer_auth)
        if raise_for_status:
            response.raise_for_status()

        return response

    def post(self, url, data=None, raise_for_status=True, timeout=5) -> requests.Response:
        response = requests.post(
            url,
            json=data,
            auth=self.bearer_auth,
            timeout=timeout
        )
        if response.status_code >= 400:
            LOGGER.error(response.text)

        if raise_for_status:
            response.raise_for_status()

        return response

    @staticmethod
    def generate_partner_lead(
        fullname, phone, birth_date, birth_month, birth_year,
        gender, national_id, issue_date, issue_month, issue_year,
        marital_status, source, email_id, monthly_net_take_home_salary,
        company_type, job_type, company_name, employment_type, occupation_type,
        job_title_type, current_work_experience_in_months, total_work_experience_in_months,
        home_address_number, home_address_street, home_address_state, home_address_city,
        current_city, home_address_ward, residence_type,
        current_city_duration_in_months, current_home_address_duration_in_months,
        raise_for_status=False
    ) -> requests.Response:
        data = {
            "fullname": fullname,
            "phone": phone,
            "birth_date": birth_date,
            "birth_month": birth_month,
            "birth_year": birth_year,
            "gender": gender,
            "national_id": national_id,
            "issue_date": issue_date,
            "issue_month": issue_month,
            "issue_year": issue_year,
            "marital_status": marital_status,
            "source": source,
            "email_id": email_id,
            "monthly_net_take_home_salary": monthly_net_take_home_salary,
            "company_type": company_type,
            "job_type": job_type,
            "company_name": company_name,
            "employment_type": employment_type,
            "occupation_type": occupation_type,
            "job_title_type": job_title_type,
            "current_work_experience_in_months": current_work_experience_in_months,
            "total_work_experience_in_months": total_work_experience_in_months,
            "home_address_number": home_address_number,
            "home_address_street": home_address_street,
            "home_address_state": home_address_state,
            "home_address_city": home_address_city,
            "current_city": current_city,
            "home_address_ward": home_address_ward,
            "residence_type": residence_type,
            "current_city_duration_in_months": current_city_duration_in_months,
            "current_home_address_duration_in_months": current_home_address_duration_in_months
        }

        # TODO: should we move this validation to submit API?
        serializer = PartnerLeadSerializer(data=data)
        request_data = serializer.moneytab_serialized_data
        LOGGER.info("Generate partner lead with user data = {}".format(json.dumps(request_data)))

        mt_requester = MoneyTabRequester()
        response = mt_requester.post(
            url=MoneyTabEndPoints.PARTNER_LEAD_GENERATION_API,
            data=request_data,
            raise_for_status=raise_for_status
        )

        return response

    @staticmethod
    def retrieve_partner_customer_by_id(customer_id, source):
        request_data = {
            "customerId": customer_id,
            "source": source
        }
        mt_requester = MoneyTabRequester()
        response = mt_requester.post(url=MoneyTabEndPoints.PARTNER_CUSTOMER_DETAIL_API, data=request_data)

        return response

    @staticmethod
    def retrieve_partner_customer_by_phone(phone, source):
        request_data = {
            "phone": phone,
            "source": source
        }
        mt_requester = MoneyTabRequester()
        response = mt_requester.post(url=MoneyTabEndPoints.PARTNER_CUSTOMER_DETAIL_API, data=request_data)

        return response

    @staticmethod
    def retrieve_partner_customer_by_email(email, source):
        request_data = {
            "email": email,
            "source": source
        }
        mt_requester = MoneyTabRequester()
        response = mt_requester.post(url=MoneyTabEndPoints.PARTNER_CUSTOMER_DETAIL_API, data=request_data)

        return response
