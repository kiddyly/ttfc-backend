from django.conf import settings
from rest_framework.routers import SimpleRouter, DefaultRouter

from .views.ticket import TicketModelViewSet
from .views.product import ProductsViewSet
from .views.activity import ActivitiesView, CallStatusViewSet
from .views.lead import (
    LeadViewSet, LeadActivityViewSet, LeadStatusViewSet
)
from .views.customer import CustomersViewSet
from .views.moneytab import (
    MoneyTabProvinceViewSet,
    MoneyTabDistrictViewSet,
    MoneyTabWardViewSet
)

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("tickets", TicketModelViewSet)
router.register("products", ProductsViewSet)
router.register("activities", ActivitiesView)
router.register("call-status", CallStatusViewSet)
router.register("leads", LeadViewSet)
router.register("lead-status", LeadStatusViewSet)
router.register("applyings", LeadActivityViewSet)
router.register("customers", CustomersViewSet)
router.register("moneytab/provinces", MoneyTabProvinceViewSet)
router.register("moneytab/districts", MoneyTabDistrictViewSet)
router.register("moneytab/wards", MoneyTabWardViewSet)
