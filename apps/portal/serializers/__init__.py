# flake8: noqa
from .serializers import ActivityWriteSerializer, ActivityReadSerializer
from .serializers import CustomerSerializer
from .serializers import FilterSerializer
from .serializers import ProductSerializer
from .serializers import TicketSerializer
from .serializers import CallStatusSerializer
