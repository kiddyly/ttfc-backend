from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.portal.models.lead import Lead, LeadActivity, LeadStatus
from apps.portal.models.customer import Customer
from apps.portal.models.product import Product
from apps.portal.models.activity import Activity, CallStatus

User = get_user_model()


class LeadCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ["id_number", "name", "gender", "age"]


class LeadUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["email", "auth0_id"]


class LeadProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "name"]


class CallStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallStatus
        fields = "__all__"


class ActivitySerializer(serializers.ModelSerializer):
    call_status = CallStatusSerializer()

    class Meta:
        model = Activity
        fields = "__all__"


class LeadStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeadStatus
        fields = "__all__"


class LeadReadSerializer(serializers.ModelSerializer):
    customer = LeadCustomerSerializer()
    created_by = LeadUserSerializer()
    approved_by = LeadUserSerializer()
    assignee = LeadUserSerializer()
    product = LeadProductSerializer()
    created_from = ActivitySerializer()
    ttfc_status = LeadStatusSerializer()

    class Meta:
        model = Lead
        fields = "__all__"


class LeadWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        fields = "__all__"


class LeadActivitySerializer(serializers.ModelSerializer):
    lead = LeadReadSerializer()

    class Meta:
        model = LeadActivity
        fields = "__all__"
