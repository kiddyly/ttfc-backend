from rest_framework import serializers

from apps.portal.models.moneytab import MoneyTabProvince
from apps.portal.models.moneytab import MoneyTabDistrict
from apps.portal.models.moneytab import MoneyTabWard


class MoneyTabProvinceSerializer(serializers.ModelSerializer):

    class Meta:
        model = MoneyTabProvince
        fields = ["id", "name", "code", ]


class MoneyTabDistrictSerializer(serializers.ModelSerializer):

    class Meta:
        model = MoneyTabDistrict
        fields = ["id", "name", "code", "province", ]


class MoneyTabWardSerializer(serializers.ModelSerializer):

    class Meta:
        model = MoneyTabWard
        fields = ["id", "name", "code", "district", ]
