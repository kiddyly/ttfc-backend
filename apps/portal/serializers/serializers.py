from rest_framework import serializers
from rest_framework.exceptions import ParseError

from apps.portal.models.customer import Customer
from apps.portal.models.filter import TableFilter
from apps.portal.models.product import Product
from apps.portal.models.ticket import Ticket
from apps.portal.models.activity import Activity, CallStatus


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = "__all__"


class FilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = TableFilter
        fields = ["table_id", "filter_field_name", "filter_field_value"]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"


class TicketSerializer(serializers.ModelSerializer):
    requester = serializers.CharField(
        source="requester.username", read_only=True, default=""
    )

    class Meta:
        model = Ticket
        fields = "__all__"

    def create(self, validated_data):
        status = validated_data.get("status")

        if status is not Ticket.PENDING:
            raise ParseError(
                detail="Not allow to create ticket has status differ than Pending",
                code="not_create_non_pending_ticket",
            )
            # ttfc_8: Automatically approve the pending ticket
            # Could be updated later following business flow
            validated_data["status"] = Ticket.APPROVED

        return Ticket.objects.create(
            **validated_data, requester=self.context.get("request").user
        )


class ActivityCustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ["id_number", "name", "gender", "age"]


class ActivityWriteSerializer(serializers.ModelSerializer):
    doer = serializers.CharField(source="doer.username", read_only=True, default="")
    customer = ActivityCustomerSerializer()

    class Meta:
        model = Activity
        fields = "__all__"


class CallStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallStatus
        fields = "__all__"


class ActivityReadSerializer(serializers.ModelSerializer):
    doer = serializers.CharField(source="doer.email", read_only=True, default="")
    customer = ActivityCustomerSerializer()
    call_status = CallStatusSerializer()

    class Meta:
        model = Activity
        fields = "__all__"
