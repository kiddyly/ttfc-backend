from django.db import transaction

from apps.portal.models.lead import Lead, LeadActivity
from apps.portal.models.product import Product
from .signals import activity_agreed


def create_lead(sender, instance, **kwargs):
    """Signal to create lead when an activity was agreed
    If there is only one product in system,
    it will auto assign to staff
    """
    data = kwargs.get("data")
    with transaction.atomic():
        if data:
            lead, _ = Lead.objects.get_or_create(**data)
            if kwargs.get("auto_assign_product"):
                if Product.objects.all().count() == 1:
                    product = Product.objects.order_by().first()
                    lead.product = product
                    lead.save()
                    for activity in product.activities.values():
                        LeadActivity.objects.create(
                            **{
                                "doer": lead.assignee if lead.assignee else None,
                                "status": LeadActivity.INPROGRESS
                                if lead.assignee
                                else LeadActivity.PEDNING,
                                "name": activity["name"],
                                "lead": lead,
                                "order": activity["order"],
                            }
                        )


activity_agreed.connect(create_lead)
