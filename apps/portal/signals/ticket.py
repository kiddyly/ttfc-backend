import logging
from django.db import transaction
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.conf import settings

from apps.portal.models.ticket import Ticket
from apps.portal.models.activity import Activity
from apps.portal.models.customer import Customer
from apps.portal.enums import Flows
from apps.portal.utils import unassigned_customers_queryset

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Ticket, dispatch_uid="ticket_auto_approval")
def ticket_auto_approval(sender, instance, created, **kwargs):
    if created:
        customers = unassigned_customers_queryset().select_for_update(
            skip_locked=True, of=("self",)
        )[: settings.DEFAULT_NUMBER_OF_LEAD_GENERATING_ACTIVITIES]

        with transaction.atomic():
            for customer in customers:
                activity = Activity.objects.create(
                    doer=instance.requester,
                    flow=Flows.LEAD_GEN,
                    customer=customer,
                    status=Activity.INPROGRESS,
                    name="Lead generating activity",
                )

                # Save a reference to the latest call activity of this customer
                customer.latest_call_activity = activity
                customer.save()
