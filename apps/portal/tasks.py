import os
import logging
import pytz
from datetime import datetime
from celery import shared_task
from openpyxl import load_workbook

from .models.customer import Customer, USER_DATA_HEADER_COLS
from .models.filter import TableFilter
from .models.activity import Activity
from .models.lead import Lead, LeadStatus
from apps.portal.moneytab.utils import MoneyTabRequester

logger = logging.getLogger(__name__)


# Celery functions for background worker
@shared_task(name="tasks.mail")
def send_mail():
    """
    Send confirm mail when done to upload all records
    """
    pass


@shared_task(name="tasks.save_to_db")
def save_to_db(file_path):
    """
    Read uploaded file from media path and parse records line by line
    """

    try:
        workbook = load_workbook(filename=file_path, data_only=True)
        sheet = workbook.active
        rows = sheet.rows

        headers = next(rows)
        index_mapper = {title.value: title.column - 1 for title in headers}

        customer_dict = {}
        for row in rows:
            for column_name in USER_DATA_HEADER_COLS.keys():
                column_index = index_mapper.get(column_name, None)
                if column_index is None:
                    continue

                try:
                    cell = row[column_index]
                    formater = USER_DATA_HEADER_COLS[column_name]["formater"] or str
                    field_name = USER_DATA_HEADER_COLS[column_name]["field"]
                    is_filterable = USER_DATA_HEADER_COLS[column_name]["filter"]
                    customer_dict[field_name] = formater(cell.value)

                    if is_filterable:
                        # Define filter for table: frontend/src/app/main/apps/data-uploader/constants.js:CustomerTableId
                        TableFilter.objects.update_or_create(
                            table_id="customer_table_id",
                            filter_field_name=field_name,
                            filter_field_value=customer_dict[field_name],
                        )
                except Exception:
                    continue

            id_number = customer_dict.get("id_number")
            if not id_number:
                continue

            # Only create new uploaded customer, keep current customer as is
            try:
                Customer.objects.get_or_create(**customer_dict)
            except Exception as exception:
                logger.error(exception)

    except Exception as ex:
        logger.error(str(ex))
    finally:
        os.remove(file_path)


@shared_task(name="tasks.clean_lead_generating")
def clean_lead_generating():
    """
    Clean up today Lead Generating
    """
    try:
        activities = Activity.objects.filter(
            created_at__lte=datetime.utcnow().replace(tzinfo=pytz.utc),
            status=Activity.INPROGRESS,
            call_status__isnull=True,
        )
        if activities:
            activities.delete()
    except Exception as e:
        logger.error(e)


@shared_task(name="tasks.update_status_partner_lead")
def update_status_partner_lead():
    """
    Get latest status of partner leads that not success yet.
    """
    try:
        leads = Lead.objects.filter(ttfc_status__status=LeadStatus.SUBMITTED,)
        for lead in leads:
            if lead.product_metadata:
                customer_id = lead.product_metadata[Lead.PRODUCT_MONEYTAB_KEY].get(
                    "customer_id"
                )
                source = lead.product_metadata[Lead.PRODUCT_MONEYTAB_KEY].get("source")
                customer_status = lead.product_metadata[Lead.PRODUCT_MONEYTAB_KEY].get(
                    "customer_status"
                )
                current_status = lead.product_metadata[Lead.PRODUCT_MONEYTAB_KEY].get(
                    "current_status"
                )
                logger.info(
                    "Updating status for lead: {}. CustomerID: {}. Source: {}".format(
                        lead, customer_id, source
                    )
                )
                if not customer_id or not source:
                    logger.debug("No Customer ID or no Source")
                    continue
                try:
                    response = MoneyTabRequester.retrieve_partner_customer_by_id(
                        customer_id, source
                    )
                    res_json = response.json()
                    if response.ok:
                        if (
                            res_json.get("finalApprovalStatus") != customer_status
                            or res_json.get("currentStatus") != current_status
                        ):
                            final_status = res_json.get("finalApprovalStatus")
                            lead.set_moneytab_product_metadata(
                                customer_id=customer_id,
                                customer_status=final_status,
                                current_status=res_json.get("currentStatus"),
                                updated_at=datetime.utcnow().isoformat(),
                                source=source,
                            )
                            if final_status in ["success", "rejected"]:
                                done_status = LeadStatus.objects.filter(
                                    status=LeadStatus.DONE
                                ).first()
                                lead.ttfc_status = done_status

                            logger.info(
                                "Updated status for lead: {}. New status is {}".format(
                                    lead, final_status
                                )
                            )
                            lead.save()
                    else:
                        error_msg = "[MONEYTAB][GET-LEAD-STATUS][FAILED][{error_code}] lead={lead_id} - {error_mess}".format(  # NOQA
                            error_code=res_json["error"]["errorCode"],
                            lead_id=lead.id,
                            error_mess=res_json["error"]["message"],
                        )
                        logger.error(error_msg)
                except Exception as e:
                    error_msg = "[MONEYTAB][GET-LEAD-STATUS][FAILED][] lead={lead_id} - {error_mess}".format(
                        lead_id=lead.id, error_mess=str(e)
                    )
                    logger.error(error_msg)

    except Exception as e:
        logger.error(e, exc_info=True)
