import logging
from django.test import TestCase

from apps.portal.moneytab.utils import MoneyTabRequester


LOGGER = logging.getLogger(__name__)


class TestMoneyTabRequester(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_generate_partner_lead(self):
        response = MoneyTabRequester.generate_partner_lead(
            "LÊ TRÚC MY", "896442020", "14", "3", "1992",
            "FEMALE", "352773358", "12", "2", "2014", "Độc thân",
            "DSA-100-A01", "letrucmy3358@gmail.com", "12000000", "OTHER",
            "SALARIED", "CÔNG TY TNHH DỆT MAY BÌNH MINH", "FULL_TIME", "HANDICRAFT",
            "EMPLOYEE_LEVEL_WORKERS", "24", "24", "285", "Bông Sao", "TP Hồ Chí Minh",
            "Quận 8", "Quận 8", "Phường 05", "UNKNOWN", "48", "48"
        )
        self.assertEqual(response.status_code, 200)
        json_data = response.json()

        self.assertEqual(json_data["phone"], '896442020')
        self.assertEqual(json_data["appStatus"], 'OPEN')
        self.assertEqual(json_data["tcAccepted"], False)
        self.assertEqual(json_data["privacyPolicyAccepted"], False)
        self.assertEqual(json_data["computeApplicationState"], True)
        self.assertEqual(json_data["reApplyApplicable"], False)
        self.assertEqual(json_data["customerId"], '438c7446-e5c3-4745-ab40-607fa2b95ade')
        self.assertEqual(json_data["trueProfileVerified"], False)

    def test_retrieve_partner_customer_by_id(self):
        response = MoneyTabRequester.retrieve_partner_customer_by_id(
            '438c7446-e5c3-4745-ab40-607fa2b95ade', 'DSA-100-A01'
        )
        self.assertEqual(response.status_code, 200)
        json_data = response.json()
        LOGGER.info(json_data)

        self.assertEqual(json_data["currentStatus"], 'consent_pending')

    def test_retrieve_partner_customer_by_phone(self):
        response = MoneyTabRequester.retrieve_partner_customer_by_phone(
            '896442020', 'DSA-100-A01'
        )
        self.assertEqual(response.status_code, 200)
        json_data = response.json()
        LOGGER.info(json_data)

        self.assertEqual(json_data["currentStatus"], 'consent_pending')

    def test_retrieve_partner_customer_by_email(self):
        response = MoneyTabRequester.retrieve_partner_customer_by_email(
            'letrucmy3358@gmail.com', 'DSA-100-A01'
        )
        self.assertEqual(response.status_code, 200)
        json_data = response.json()
        LOGGER.info(json_data)

        self.assertEqual(json_data["currentStatus"], 'consent_pending')
