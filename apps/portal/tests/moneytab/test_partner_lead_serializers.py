import logging
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from apps.portal.moneytab.serializers import PartnerLeadSerializer


LOGGER = logging.getLogger(__name__)


class TestPartnerLeadSerializer(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_validate_fullname(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_fullname("Lê Trúc My")
        self.assertEqual(value, "LÊ TRÚC MY")

    def test_validate_phone(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_phone("896442020")
        self.assertEqual(value, "896442020")

    def test_validate_birth_date(self):
        serializer = PartnerLeadSerializer()

        for i in range(1, 32):
            value1 = serializer.validate_birth_date("{}".format(i))
            self.assertEqual(value1, "{}".format(i))

        try:
            serializer.validate_birth_date("0")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Invalid day of month. Required 0 < birth_date <= 31")
            self.assertEqual(e.detail[0].code, "invalid")

        try:
            serializer.validate_birth_date("32")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Invalid day of month. Required 0 < birth_date <= 31")
            self.assertEqual(e.detail[0].code, "invalid")

        try:
            serializer.validate_birth_date("a")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Field 'birth_date' required a number as input")
            self.assertEqual(e.detail[0].code, "invalid")

    def test_validate_birth_month(self):
        serializer = PartnerLeadSerializer()

        for i in range(1, 13):
            value1 = serializer.validate_birth_month("{}".format(i))
            self.assertEqual(value1, "{}".format(i))

        try:
            serializer.validate_birth_month("0")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Invalid month. Required 0 < birth_month <= 12")
            self.assertEqual(e.detail[0].code, "invalid")

        try:
            serializer.validate_birth_month("13")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Invalid month. Required 0 < birth_month <= 12")
            self.assertEqual(e.detail[0].code, "invalid")

        try:
            serializer.validate_birth_month("a")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Field 'birth_month' required a number as input")
            self.assertEqual(e.detail[0].code, "invalid")

    def test_validate_birth_year(self):
        serializer = PartnerLeadSerializer()

        for i in range(1900, 2020):
            value1 = serializer.validate_birth_year("{}".format(i))
            self.assertEqual(value1, "{}".format(i))

        try:
            serializer.validate_birth_year("0")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Invalid year. Required birth_year > 0")
            self.assertEqual(e.detail[0].code, "invalid")

        try:
            serializer.validate_birth_year("a")
        except ValidationError as e:
            self.assertEqual(e.detail[0], "Field 'birth_year' required a number as input")
            self.assertEqual(e.detail[0].code, "invalid")

    def test_validate_gender(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_gender("FEMALE")
        self.assertEqual(value, "FEMALE")

    def test_validate_national_id(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_national_id("352773358")
        self.assertEqual(value, "352773358")

    def test_validate_issue_date(self):
        serializer = PartnerLeadSerializer()
        for i in range(1, 32):
            value1 = serializer.validate_issue_date("{}".format(i))
            self.assertEqual(value1, "{}".format(i))

    def test_validate_issue_month(self):
        serializer = PartnerLeadSerializer()
        for i in range(1, 13):
            value1 = serializer.validate_issue_month("{}".format(i))
            self.assertEqual(value1, "{}".format(i))

    def test_validate_issue_year(self):
        serializer = PartnerLeadSerializer()
        for i in range(1900, 2020):
            value1 = serializer.validate_issue_year("{}".format(i))
            self.assertEqual(value1, "{}".format(i))

    def test_validate_marital_status(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_marital_status("Độc thân")
        self.assertEqual(value, "Độc thân")

    def test_validate_source(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_source("source")
        self.assertEqual(value, "source")

    def test_validate_email_id(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_email_id("letrucmy3358@gmail.com")
        self.assertEqual(value, "letrucmy3358@gmail.com")

    def test_validate_monthly_net_take_home_salary(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_monthly_net_take_home_salary("12000000")
        self.assertEqual(value, "12000000")

    def test_validate_company_type(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_company_type("OTHER")
        self.assertEqual(value, "OTHER")

    def test_validate_job_type(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_job_type("SALARIED")
        self.assertEqual(value, "SALARIED")

    def test_validate_company_name(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_company_name("CÔNG TY TNHH DỆT MAY BÌNH MINH")
        self.assertEqual(value, "CÔNG TY TNHH DỆT MAY BÌNH MINH")

    def test_validate_employment_type(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_employment_type("FULL_TIME")
        self.assertEqual(value, "FULL_TIME")

    def test_validate_occupation_type(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_occupation_type("Thủ công nghiệp")
        self.assertEqual(value, "Thủ công nghiệp")

    def test_validate_job_title_type(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_job_title_type("Công nhân cấp nhân viên")
        self.assertEqual(value, "Công nhân cấp nhân viên")

    def test_validate_current_work_experience_in_months(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_current_work_experience_in_months("24")
        self.assertEqual(value, "24")

    def test_validate_total_work_experience_in_months(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_total_work_experience_in_months("24")
        self.assertEqual(value, "24")

    def test_validate_home_address_number(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_home_address_number("285")
        self.assertEqual(value, "285")

    def test_validate_home_address_street(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_home_address_street("Bông Sao")
        self.assertEqual(value, "Bông Sao")

    def test_validate_home_address_state(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_home_address_state("TP Hồ Chí Minh")
        self.assertEqual(value, "TP Hồ Chí Minh")

    def test_validate_home_address_city(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_home_address_city("Quận 8")
        self.assertEqual(value, "Quận 8")

    def test_validate_current_city(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_current_city("Quận 8")
        self.assertEqual(value, "Quận 8")

    def test_validate_home_address_ward(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_home_address_ward("Phường 05")
        self.assertEqual(value, "Phường 05")

    def test_validate_residence_type(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_residence_type("UNKNOWN")
        self.assertEqual(value, "UNKNOWN")

    def test_validate_current_city_duration_in_months(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_current_city_duration_in_months("48")
        self.assertEqual(value, "48")

    def test_validate_current_home_address_duration_in_months(self):
        serializer = PartnerLeadSerializer()
        value = serializer.validate_current_home_address_duration_in_months("48")
        self.assertEqual(value, "48")

    def test_full_validation(self):
        data = {
            "fullname": "LÊ TRÚC MY",
            "phone": "896442020",
            "birth_date": "14",
            "birth_month": "3",
            "birth_year": "1992",
            "gender": "FEMALE",
            "national_id": "352773358",
            "issue_date": "12",
            "issue_month": "2",
            "issue_year": "2014",
            "marital_status": "Độc thân",
            "email_id": "letrucmy3358@gmail.com",
            "monthly_net_take_home_salary": "12000000",
            "company_type": "OTHER",
            "job_type": "SALARIED",
            "company_name": "CÔNG TY TNHH DỆT MAY BÌNH MINH",
            "employment_type": "FULL_TIME",
            "occupation_type": "Thủ công nghiệp",
            "job_title_type": "Công nhân cấp nhân viên",
            "current_work_experience_in_months": "24",
            "total_work_experience_in_months": "24",
            "home_address_number": "285",
            "home_address_street": "Bông Sao",
            "home_address_state": "TP Hồ Chí Minh",
            "home_address_city": "Quận 8",
            "current_city": "Quận 8",
            "home_address_ward": "Phường 05",
            "residence_type": "UNKNOWN",
            "current_city_duration_in_months": "48",
            "current_home_address_duration_in_months": "48"
        }
        serializer = PartnerLeadSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.assertDictContainsSubset(data, serializer.validated_data)

    def test_property_moneytab_serialized_data(self):
        data = {
            "fullname": "LÊ TRÚC MY",
            "phone": "896442020",
            "birth_date": "14",
            "birth_month": "3",
            "birth_year": "1992",
            "gender": "FEMALE",
            "national_id": "352773358",
            "issue_date": "12",
            "issue_month": "2",
            "issue_year": "2014",
            "marital_status": "Độc thân",
            "email_id": "letrucmy3358@gmail.com",
            "monthly_net_take_home_salary": "12000000",
            "company_type": "OTHER",
            "job_type": "SALARIED",
            "company_name": "CÔNG TY TNHH DỆT MAY BÌNH MINH",
            "employment_type": "FULL_TIME",
            "occupation_type": "Thủ công nghiệp",
            "job_title_type": "Công nhân cấp nhân viên",
            "current_work_experience_in_months": "24",
            "total_work_experience_in_months": "24",
            "home_address_number": "285",
            "home_address_street": "Bông Sao",
            "home_address_state": "TP Hồ Chí Minh",
            "home_address_city": "Quận 8",
            "current_city": "Quận 8",
            "home_address_ward": "Phường 05",
            "residence_type": "UNKNOWN",
            "current_city_duration_in_months": "48",
            "current_home_address_duration_in_months": "48"
        }

        request_data = {
            "fullname": "LÊ TRÚC MY",
            "phone": "896442020",
            "birth_Date": "14",
            "birth_Month": "3",
            "birth_Year": "1992",
            "gender": "FEMALE",
            "nationalId": "352773358",
            "issue_Date": "12",
            "issue_Month": "2",
            "issue_Year": "2014",
            "maritalStatus": "Độc thân",
            "source": "DSA-100-A01",
            "emailId": "letrucmy3358@gmail.com",
            "monthlyNetTakeHomeSalary": "12000000",
            "companyType": "OTHER",
            "jobType": "SALARIED",
            "companyName": "CÔNG TY TNHH DỆT MAY BÌNH MINH",
            "employmentType": "FULL_TIME",
            "occupationType": "Thủ công nghiệp",
            "jobTitleType": "Công nhân cấp nhân viên",
            "currentWorkExperienceInMonths": "24",
            "totalWorkExperienceInMonths": "24",
            "homeAddressNumber": "285",
            "homeAddressStreet": "Bông Sao",
            "homeAddressState": "TP Hồ Chí Minh",
            "homeAddressCity": "Quận 8",
            "currentCity": "Quận 8",
            "homeAddressWard": "Phường 05",
            "residenceType": "UNKNOWN",
            "currentCityDurationInMonths": "48",
            "currentHomeAddressDurationInMonths": "48"
        }
        serializer = PartnerLeadSerializer(data=data)
        self.assertDictEqual(serializer.moneytab_serialized_data, request_data)
