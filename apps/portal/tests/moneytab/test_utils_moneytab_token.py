from django.test import TestCase

from apps.portal.moneytab.utils import MoneyTabToken


class TestMoneyTabToken(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_encoded_base64_from_string(self):
        s1 = "abc"
        expected_s1_b64 = 'YWJj'
        result_s1 = MoneyTabToken.get_encoded_base64_from_string(s1)
        self.assertEqual(result_s1, expected_s1_b64)

        s2 = "ttfc:GjNHO5CzUSm95NpnPjKO5iq1W98MJQ0f"
        expected_s2_b64 = 'dHRmYzpHak5ITzVDelVTbTk1TnBuUGpLTzVpcTFXOThNSlEwZg=='
        result_s2 = MoneyTabToken.get_encoded_base64_from_string(s2)
        self.assertEqual(result_s2, expected_s2_b64)

    def test_get_basic_token(self):
        expected_b64 = 'dHRmYzpHak5ITzVDelVTbTk1TnBuUGpLTzVpcTFXOThNSlEwZg=='
        result = MoneyTabToken.get_basic_token("ttfc", "GjNHO5CzUSm95NpnPjKO5iq1W98MJQ0f")
        self.assertEqual(result, expected_b64)

    def test_get_basic_token_mt_client_id_is_none(self):
        try:
            MoneyTabToken.get_basic_token(None, "GjNHO5CzUSm95NpnPjKO5iq1W98MJQ0f")
        except AssertionError as e:
            self.assertEqual(str(e), "MONEYTAB_CLIENT_ID is None")

    def test_get_basic_token_mt_client_secret_is_none(self):
        try:
            MoneyTabToken.get_basic_token("ttfc", None)
        except AssertionError as e:
            self.assertEqual(str(e), "MONEYTAB_CLIENT_SECRET is None")

    def test_request_bearer_token_from_moneytab_success(self):
        res_json = MoneyTabToken.request_bearer_token_from_moneytab(
            api_uri="https://dev.moneytap.vn/oauth/token?grant_type=client_credentials"
        )
        self.assertIsInstance(res_json, dict)
        self.assertIn("access_token", res_json.keys())
        self.assertIn("token_type", res_json.keys())
        self.assertIn("expires_in", res_json.keys())
        self.assertIn("scope", res_json.keys())

    def test_request_bearer_token_from_moneytab_raise_request_exception(self):

        # Connection Exception
        try:
            MoneyTabToken.request_bearer_token_from_moneytab(
                api_uri="http://localhost/oauth/token?grant_type=client_credentials"
            )
        except Exception as e:
            self.assertIsInstance(e, Exception)

        # Response return status code 401 Exception
        try:
            MoneyTabToken.request_bearer_token_from_moneytab(
                api_uri="https://dev.moneytap.vn/oauth/token?grant_type=client_credentials",
                mt_client_id="mt_client_id",
                mt_client_secret="mt_client_secret"
            )
        except Exception as e:
            self.assertIsInstance(e, Exception)

    def test_object_initialize_success(self):
        moneytab_token = MoneyTabToken()
        self.assertIsNotNone(moneytab_token.access_token)
        self.assertGreater(moneytab_token.expires_in, 0)
        self.assertIsNotNone(moneytab_token.scope)
        self.assertIsNotNone(moneytab_token.token_type)

    def test_get_bearer_token(self):
        bearer_token = MoneyTabToken.get_bearer_token()
        self.assertIsNotNone(bearer_token)
