from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase, APIClient
from rest_framework import status

from apps.portal.models.ticket import Ticket


User = get_user_model()


class TicketTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            email="testuser@portalfv.com",
            password="P@ssw0rd123",
            auth0_id="1",
            roles=[],
        )
        self.client.force_authenticate(self.user)

    def test_get_tickets(self):
        Ticket.objects.create(
            name="Request activities",
            description="Create lead generating activities",
            status=Ticket.PENDING,
            requester=self.user,
        )
        response = self.client.get("/api/tickets/", format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_create_ticket_successful(self):
        name = "Request activities"
        description = "Create lead generating activities"

        response = self.client.post(
            "/api/tickets/",
            {"name": name, "description": description, "status": Ticket.PENDING},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Ticket.objects.count(), 1)

        ticket = Ticket.objects.first()
        self.assertDictContainsSubset(
            response.data,
            {
                "id": ticket.id,
                "name": name,
                "description": description,
                "status": Ticket.PENDING,
                "requester": self.user.username,
                "created_at": ticket.created_at.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                "updated_at": ticket.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            },
        )

    def test_create_not_pending_ticket(self):
        name = "Request activities"
        description = "Create lead generating activities"

        response = self.client.post(
            "/api/tickets/",
            {"name": name, "description": description, "status": Ticket.APPROVED},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["detail"],
            "Not allow to create ticket has status differ than Pending",
        )
