from django.urls import path
from apps.portal.views.filter import ListFiltersView
from apps.portal.router import router
from apps.auth0.urls import urlpatterns as auth0url


urlpatterns = [
    path("filters", ListFiltersView.as_view()),
]

urlpatterns += router.urls
urlpatterns += auth0url
