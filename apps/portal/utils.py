import itertools
from datetime import datetime, timedelta

from django.db.models import Q

from apps.portal.models.customer import Customer


def unassigned_customers_queryset():
    manual_pool = Customer.objects.filter(status=Customer.MANUAL_POOL)

    no_acvitity = Q(latest_call_activity__isnull=True)

    two_month_timedelta = datetime.now() - timedelta(days=60)
    reusable_data = Q(latest_call_activity__call_status__allow_reuse=True) & Q(
        latest_call_activity__updated_at__lte=two_month_timedelta
    )

    return manual_pool.filter(no_acvitity | reusable_data)


def assign_leads_for_staffs_auto(staffs: list, unassigned_leads: list):
    """This function is implement an algorithm that pick lead from available leads and
    assign it to 1 user in staffs list. For example:
    - staffs = [user1, user2]
    - unassigned_leads = [lead1, lead2, lead3, lead4, lead5]
    - The return list should be: [(user1, lead1), (user2, lead2), (user1, lead3),
                                 (user2, lead4), (user1, lead5), (user2, None)]

    :param staffs: List of user instances are waiting to assigned multiple leads
    :type staffs: list (django queryset)
    :param unassigned_leads: List of available leads
    :type unassigned_leads: list (django queryset)
    :return: List of tuple (user_obj, lead_obj), in that lead_obj
             is offer to assign for user_obj, lead_obj can be None
    :rtype: list
    """
    total_staff = step = staffs.count()
    staff_lead_pairs = [itertools.product([staffs[i]], unassigned_leads[i::step]) for i in range(0, total_staff)]
    return itertools.chain.from_iterable(staff_lead_pairs)

