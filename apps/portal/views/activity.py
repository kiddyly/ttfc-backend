import pytz
from datetime import datetime

from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q
from rest_framework import filters
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.portal.models.activity import Activity, CallStatus
from apps.portal.serializers import (
    ActivityReadSerializer,
    ActivityWriteSerializer,
    CallStatusSerializer,
)
from apps.portal.filters.activity import ActivityFilter, CallStatusFilter
from apps.auth0.permissions import RequiredRolesOrReadOnly
from apps.portal.serializers.base import ReadWriteSerializerMixin
from apps.portal.signals.signals import activity_agreed


class ActivitiesView(ReadWriteSerializerMixin, viewsets.ModelViewSet):
    """
    Provides ViewSet for Activity model
    """

    queryset = Activity.objects.all().select_related("doer", "customer").only(
        'call_status',
        'created_at',
        'updated_at',
        'flow',
        'name',
        'notes',
        'status',
        'customer__age',
        'customer__gender',
        'customer__id_number',
        'customer__name',
        'doer__email',
    )
    write_serializer_class = ActivityWriteSerializer
    read_serializer_class = ActivityReadSerializer
    filter_backends = [
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = ActivityFilter
    search_fields = "__all__"
    ordering_fields = "__all__"

    def get_queryset(self):
        activity_query = super().get_queryset()

        if self.request.user.is_superuser:
            return activity_query[0:200]
        elif "collaborator" in self.request.user.roles:
            user_timezone = pytz.timezone(self.request.user.timezone)
            user_time = datetime.now(user_timezone)
            start_time = user_time.replace(hour=0, minute=0, second=0, microsecond=0)
            end_time = user_time.replace(hour=23, minute=59, second=59)

            where_doer = Q(doer=self.request.user)
            where_today_activities = Q(
                created_at__gte=start_time.astimezone(pytz.utc)
            ) & Q(created_at__lte=end_time.astimezone(pytz.utc))
            where_onhold_activities = Q(call_status__status=CallStatus.ONHOLD)

            return activity_query.filter(
                where_doer & (where_today_activities | where_onhold_activities)
            )
        return None

    @action(detail=True, methods=["patch"])
    def agreed(self, request, pk=None):
        """Extra action that called when the activity is agreed
        When an activity is agreed, we should send a signal activity_agreed
        After that a lead will be automatically create from this activity

        Args:
            request (Request): Request from portal
            pk (int): The primary key of current object. Defaults to None.

        Returns:
            Response: A response with updated activity data
        """
        activity = self.get_object()
        serializer = ActivityWriteSerializer(activity, data=request.data, partial=True)
        if serializer.is_valid():
            activity = serializer.save()
            data = {
                "created_by": request.user,
                "created_from": activity,
                "customer": activity.customer,
            }
            auto_assign_product = False
            if "staff" in request.user.roles:
                data["assignee"] = request.user
                data["assigned_at"] = datetime.utcnow()
                auto_assign_product = True
            activity_agreed.send(
                sender=self.__class__,
                instance=activity,
                data=data,
                auto_assign_product=auto_assign_product
            )
            return Response(ActivityReadSerializer(activity).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CallStatusViewSet(viewsets.ModelViewSet):
    queryset = CallStatus.objects.all()
    filter_backends = [
        DjangoFilterBackend,
    ]
    serializer_class = CallStatusSerializer
    permission_classes = [
        RequiredRolesOrReadOnly(["admin"]) | RequiredRolesOrReadOnly(["manager"])
    ]
    filterset_class = CallStatusFilter
