import uuid
import logging

from django_filters.rest_framework import DjangoFilterBackend
from django.conf import settings
from rest_framework import filters, viewsets, status
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from rest_framework.decorators import action

from apps.portal.models.customer import Customer, USER_DATA_HEADER_COLS
from apps.portal.serializers import CustomerSerializer
from apps.portal.tasks import save_to_db
from apps.portal.filters.customer import CustomerFilter
from apps.portal.filters.pagination import ItemSetPagination
from apps.auth0.permissions import RequiredRoles
from apps.portal.utils import unassigned_customers_queryset

logger = logging.getLogger(__name__)


class CustomersViewSet(viewsets.ModelViewSet):
    """
    ViewSet for Customer model
    """

    queryset = Customer.objects.exclude(status__in=[Customer.DELETED])
    serializer_class = CustomerSerializer
    filter_backends = [
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = CustomerFilter
    pagination_class = ItemSetPagination
    search_fields = [item["field"] for item in USER_DATA_HEADER_COLS.values()]
    ordering_fields = [item["field"] for item in USER_DATA_HEADER_COLS.values()]

    def get_queryset(self):
        show_unassigned = self.request.query_params.get("show_unassigned", False)
        if show_unassigned:
            return unassigned_customers_queryset()

        return super().get_queryset()

    def retrieve(self, request, pk=None):
        try:
            customer = Customer.objects.get(id_number=pk)
        except Customer.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = CustomerSerializer(customer)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        try:
            customer = Customer.objects.get(id_number=pk)
        except Customer.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = CustomerSerializer(customer, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(
        detail=False,
        methods=["post"],
        permission_classes=[RequiredRoles(["admin"])],
        parser_classes=[FileUploadParser],
    )
    def upload(self, request):
        file_data = request.FILES.get("file")
        if not file_data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        file_to_save_path = "{}/customers_{}_{}".format(settings.MEDIA_ROOT, str(uuid.uuid4()), file_data.name)

        logger.info("Saving customer data file: {}".format(file_to_save_path))

        with open(file_to_save_path, "wb+") as destination:
            for chunk in file_data.chunks():
                destination.write(chunk)

            save_to_db.apply_async(args=(file_to_save_path,), queue="urgent")  # run celery worker
            return Response(
                "Uploaded customer records successfully!",
                status=status.HTTP_201_CREATED,
            )

        return Response(
            "Unable to upload customer data",
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    @action(detail=False, methods=["post"])
    def mark_all_manual_call(self, request):
        queryset = self.queryset
        filter_params = request.data
        if filter_params:
            queryfilter = self.filterset_class(filter_params, queryset=queryset)
            customers = queryfilter.qs
            customers.update(status=Customer.MANUAL_POOL)
            return Response({"count": customers.count()})

        return Response(
            "Missing filter params",
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    @action(detail=False, methods=["post"])
    def delete_all(self, request):
        queryset = self.queryset
        filter_params = request.data
        if filter_params:
            queryfilter = self.filterset_class(filter_params, queryset=queryset)
            customers = queryfilter.qs
            customers.update(status=Customer.DELETED)
            return Response(
                "Delete customer successfully!",
                status=status.HTTP_200_OK,
            )

        return Response(
            "Missing filter params",
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
