from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from ..models.filter import TableFilter
from apps.portal.serializers import FilterSerializer


class ListFiltersView(generics.ListAPIView):
    """
    Provides a get method handler
    """

    queryset = TableFilter.objects.all()
    serializer_class = FilterSerializer
    filter_backends = [DjangoFilterBackend]
    pagination_class = None
    filterset_fields = ["table_id", "filter_field_name"]
