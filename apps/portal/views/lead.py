import logging
from datetime import datetime
from django.db import transaction
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields.jsonb import KeyTransform, KeyTextTransform
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets, filters
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.auth0.permissions import RequiredRolesOrReadOnly
from apps.portal.models.lead import Lead, LeadActivity, LeadStatus
from apps.portal.serializers.lead_serializer import (
    LeadReadSerializer,
    LeadWriteSerializer,
    LeadActivitySerializer,
    LeadStatusSerializer,
)
from apps.portal.serializers.base import ReadWriteSerializerMixin
from apps.portal.models.product import Product
from apps.portal.filters.pagination import ItemSetPagination
from apps.portal.filters.lead import LeadActivityFilter, LeadFilter, LeadStatusFilter
from apps.portal.filters.order import RelatedOrderingFilter
from apps.portal.moneytab.utils import MoneyTabRequester
from apps.portal.moneytab.enums import CompanyType
from apps.portal.moneytab.enums import ResidenceType
from apps.portal.moneytab.enums import Gender
from apps.portal.utils import assign_leads_for_staffs_auto


logger = logging.getLogger(__name__)
User = get_user_model()


class LeadViewSet(ReadWriteSerializerMixin, viewsets.ModelViewSet):
    """
    Get all leads available
    """

    # TODO: should make product key more flexible
    # currently only support moneytab
    queryset = (
        Lead.objects.annotate(moneytab=KeyTransform("moneytab", "product_metadata"))
        .annotate(customer_status=KeyTextTransform("customer_status", "moneytab"))
        .select_related(
            "customer",
            "product",
            "created_by",
            "approved_by",
            "assignee",
            "ttfc_status",
            "created_from",
            "created_from__call_status",
        )
        .only(
            "customer__id_number",
            "customer__name",
            "customer__gender",
            "customer__age",
            "created_by__email",
            "created_by__auth0_id",
            "approved_by__email",
            "approved_by__auth0_id",
            "assignee__email",
            "assignee__auth0_id",
            "product__id",
            "product__name",
            "created_from",
            "ttfc_status",
            "created_at",
            "updated_at",
            "assigned_at",
            "product_metadata",
        )
        .prefetch_related("leadactivity_set")
        .order_by("-assigned_at")
    )
    read_serializer_class = LeadReadSerializer
    write_serializer_class = LeadWriteSerializer
    filter_backends = [
        DjangoFilterBackend,
        RelatedOrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = LeadFilter
    pagination_class = ItemSetPagination
    search_fields = "__all__"
    ordering_fields = ["__all_related__", "customer_status"]

    def get_queryset(self):
        queryset = super().get_queryset()

        user = self.request.user
        if "admin" in self.request.user.roles or "manager" in self.request.user.roles:
            return queryset
        else:
            return queryset.filter(assignee=user)

    def create(self, request):
        request.data["created_by"] = request.user.auth0_id
        if "staff" in request.user.roles:
            request.data["assignee"] = request.user.auth0_id
            request.data["assigned_at"] = datetime.utcnow()
        return super().create(request)

    @transaction.atomic
    @action(detail=True, methods=["patch"])
    def set_product(self, request, pk=None):
        lead = self.get_object()
        product_id = request.data.get("product")
        product = Product.objects.get(pk=product_id)
        if product:
            lead.product = product
            if lead.assignee:
                lead.approved_by = request.user
            lead.save()
            for activity in product.activities.values():
                LeadActivity.objects.create(
                    **{
                        "doer": lead.assignee if lead.assignee else None,
                        "status": LeadActivity.INPROGRESS if lead.assignee else LeadActivity.PEDNING,
                        "name": activity["name"],
                        "lead": lead,
                        "order": activity["order"],
                    }
                )
            return Response(LeadReadSerializer(lead).data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @transaction.atomic
    @action(detail=True, methods=["patch"])
    def set_assignee(self, request, pk=None):
        lead = self.get_object()
        assignee_id = request.data.get("assignee")
        assignee = User.objects.get(pk=assignee_id)
        if assignee:
            lead.assignee = assignee
            lead.assigned_at = datetime.utcnow()
            if lead.product:
                lead.approved_by = request.user
            lead.save()
            # Assign lead activities if any
            activities = LeadActivity.objects.all().filter(lead=lead)
            for activity in activities:
                activity.doer = assignee
                activity.status = LeadActivity.INPROGRESS
                activity.save()

            serializer = LeadReadSerializer(lead)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    @transaction.atomic
    @action(detail=False, methods=["post"])
    def assign_multiple_staffs(self, request):
        staff_ids: list = request.data.get("staffs", [])
        lead_per_staff: int = request.data.get("lead_per_staff")
        product_id: int = request.data.get("product")
        product = Product.objects.get(pk=product_id)
        if staff_ids and lead_per_staff:
            staffs = User.objects.filter(pk__in=staff_ids)
            total_staff = staffs.count()
            total_needing_lead = total_staff * lead_per_staff
            # get all available leads without assigned to any stuff that fix with (staffs, lead_per_staff)
            available_leads_qs = self.get_queryset()
            available_leads = available_leads_qs.filter(assignee=None).select_for_update(
                skip_locked=True, of=("self",)
            )[:total_needing_lead]

            auto_assigned_result = assign_leads_for_staffs_auto(staffs, available_leads)
            for (assignee, unassign_lead) in auto_assigned_result:
                if not unassign_lead:
                    continue

                if product:
                    unassign_lead.product = product
                    unassign_lead.approved_by = request.user
                    for activity in product.activities.values():
                        LeadActivity.objects.create(
                            **{
                                "doer": assignee,
                                "status": LeadActivity.INPROGRESS,
                                "name": activity["name"],
                                "lead": unassign_lead,
                                "order": activity["order"],
                            }
                        )

                unassign_lead.assignee = assignee
                unassign_lead.assigned_at = datetime.utcnow()
                unassign_lead.save()

                if not product:
                    # Assign lead activities if any
                    activities = LeadActivity.objects.filter(lead=unassign_lead)
                    for activity in activities:
                        activity.doer = assignee
                        activity.status = LeadActivity.INPROGRESS
                        activity.save()

            leads = self.get_queryset()
            serializer = LeadReadSerializer(leads, many=True)
            return Response(serializer.data)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["get"])
    def activities(self, request, pk=None):
        lead = self.get_object()
        activities = LeadActivity.objects.all().filter(lead=lead)
        serializer = LeadActivitySerializer(activities, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=["patch"])
    def done_activity(self, request, pk=None):
        lead = self.get_object()
        activity_id = request.data.get("activity")
        activity = LeadActivity.objects.get(pk=activity_id)

        ################################################
        # TODO: Also change status of lead to submitted
        # Because now we only have 1 product and 1 activity
        # In the future, if there are more activity
        # These line of code SHOULD BE UPDATED following
        ################################################
        submitted_status = LeadStatus.objects.filter(status=LeadStatus.SUBMITTED).first()

        source_id = request.user.source_id

        # first, we call submit customer to Moneytab first
        success, data = self._submit_to_moneytab(lead, source_id)

        if success:
            updated_at = datetime.utcnow().isoformat()
            # do 3 updates in one transaction
            with transaction.atomic():
                # https://docs.djangoproject.com/en/3.1/topics/db/transactions/#savepoints
                # transaction.on_commit(lambda: self._submit_to_moneytab(activity.lead.id, source_id))
                activity.status = LeadActivity.DONE
                activity.save()
                lead.set_moneytab_product_metadata(customer_id=data, updated_at=updated_at, source=source_id)
                lead.ttfc_status = submitted_status
                lead.save()

            logger.info(
                "[MONEYTAB][GENERATE-LEAD][SUCCESS] user={email} lead={lead_id} customer_id={customer_id}".format(
                    email=request.user.email, lead_id=lead.id, customer_id=data
                )
            )

            serializer = LeadActivitySerializer(activity)
            return Response(serializer.data)
        else:
            logger.error(
                "[MONEYTAB][GENERATE-LEAD][FAILED] user={email} lead={lead_id}".format(
                    email=request.user.email, lead_id=lead.id
                )
            )
            return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["post"])
    def pending(self, request, pk=None):
        lead = self.get_object()
        status = request.data.get("status_id")
        if lead.ttfc_status and lead.ttfc_status.status != LeadStatus.PENDING:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            pending_status = LeadStatus.objects.get(pk=status)
            lead.ttfc_status = pending_status
            lead.save()
            serializer = LeadReadSerializer(lead)
        return Response(serializer.data)

    @transaction.atomic
    @action(detail=False, methods=["post"])
    def auto_assign(self, request, pk=None):
        quantity = request.data.get("quantity")
        assignee_id = request.data.get("assignee")
        product_id = request.data.get("product")
        if assignee_id and product_id and quantity:
            product = Product.objects.get(pk=product_id)
            assignee = User.objects.get(pk=assignee_id)
            leads = Lead.objects.filter(assignee=None)[: int(quantity)]
            for lead in leads:
                lead.assignee = assignee
                lead.product = product
                lead.approved_by = request.user
                lead.assigned_at = datetime.utcnow()
                lead.save()
                # Create Lead Activity as well
                for activity in product.activities.values():
                    LeadActivity.objects.create(
                        **{
                            "doer": assignee,
                            "status": LeadActivity.INPROGRESS,
                            "name": activity["name"],
                            "lead": lead,
                            "order": activity["order"],
                        }
                    )

            serializer = LeadReadSerializer(leads, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def _submit_to_moneytab(self, lead_obj, source_id):
        if not source_id:
            return None

        customer_obj = lead_obj.customer

        name = customer_obj.name
        phone = customer_obj.phone if customer_obj.phone[0] != "0" else customer_obj.phone[1:]
        birth_date = customer_obj.date_of_birth.day
        birth_month = customer_obj.date_of_birth.month
        birth_year = customer_obj.date_of_birth.year
        gender = customer_obj.gender
        if gender == "NAM":
            gender = Gender.MALE.value
        else:
            gender = Gender.FEMALE.value

        national_id = customer_obj.id_number
        issue_date = customer_obj.issue_date.day
        issue_month = customer_obj.issue_date.month
        issue_year = customer_obj.issue_date.year
        marital_status = customer_obj.marital_status
        email_id = customer_obj.email
        monthly_net_take_home_salary = customer_obj.income
        company_type = CompanyType.OTHER.value
        job_type = customer_obj.job_type
        company_name = customer_obj.company
        employment_type = customer_obj.employment_type
        occupation_type = customer_obj.occupation_type
        job_title_type = customer_obj.job_title_type
        current_work_experience_in_months = customer_obj.current_work_experience_in_months
        total_work_experience_in_months = customer_obj.total_work_experience_in_months
        home_address_number = "0"
        home_address_street = customer_obj.street
        home_address_state = customer_obj.state
        home_address_city = customer_obj.district
        current_city = home_address_city
        home_address_ward = customer_obj.ward
        residence_type = ResidenceType.UNKNOWN.value
        current_city_duration_in_months = customer_obj.current_city_duration_in_months
        current_home_address_duration_in_months = customer_obj.current_home_address_duration_in_months

        try:
            res = MoneyTabRequester.generate_partner_lead(
                name,
                phone,
                birth_date,
                birth_month,
                birth_year,
                gender,
                national_id,
                issue_date,
                issue_month,
                issue_year,
                marital_status,
                source_id,
                email_id,
                monthly_net_take_home_salary,
                company_type,
                job_type,
                company_name,
                employment_type,
                occupation_type,
                job_title_type,
                current_work_experience_in_months,
                total_work_experience_in_months,
                home_address_number,
                home_address_street,
                home_address_state,
                home_address_city,
                current_city,
                home_address_ward,
                residence_type,
                current_city_duration_in_months,
                current_home_address_duration_in_months,
            )
            if res.ok:
                customer_id = res.json()["customerId"]
                return True, customer_id
            else:
                res_json = res.json()
                error_msg = "[MONEYTAB][{}] {}".format(res_json["error"]["errorCode"], res_json["error"]["message"])

                return False, error_msg

        except Exception as e:
            logger.error(e, exc_info=True)

        return False, "[UNKNOWN_ERROR] Generate Partner Lead failed!"


class LeadActivityViewSet(viewsets.ModelViewSet):
    """
    Get all leads activities
    """

    queryset = LeadActivity.objects.all()
    serializer_class = LeadActivitySerializer
    filter_backends = [
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = LeadActivityFilter
    pagination_class = ItemSetPagination
    search_fields = "__all__"
    ordering_fields = "__all__"


class LeadStatusViewSet(viewsets.ModelViewSet):
    """
    View Set for Lead Status model
    """

    queryset = LeadStatus.objects.all()
    serializer_class = LeadStatusSerializer
    filter_backends = [
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = LeadStatusFilter
    permission_classes = [RequiredRolesOrReadOnly(["admin"]) | RequiredRolesOrReadOnly(["manager"])]
