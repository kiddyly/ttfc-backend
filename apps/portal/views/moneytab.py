from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters

from apps.portal.models.moneytab import (
    MoneyTabProvince,
    MoneyTabDistrict,
    MoneyTabWard
)
from apps.portal.serializers.moneytab_serializer import (
    MoneyTabProvinceSerializer,
    MoneyTabDistrictSerializer,
    MoneyTabWardSerializer,
)
from apps.portal.filters.moneytab import MoneyTabDistrictFilter, MoneyTabWardFilter


class MoneyTabProvinceViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Get all MoneyTab Province available
    """
    queryset = MoneyTabProvince.objects.all().order_by('id')
    serializer_class = MoneyTabProvinceSerializer


class MoneyTabDistrictViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Get all MoneyTab Province available
    """
    queryset = MoneyTabDistrict.objects.all().order_by('-id')
    serializer_class = MoneyTabDistrictSerializer
    filter_backends = [
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = MoneyTabDistrictFilter
    search_fields = "__all__"
    ordering_fields = "__all__"


class MoneyTabWardViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Get all MoneyTab Province available
    """
    queryset = MoneyTabWard.objects.all().order_by('id')
    serializer_class = MoneyTabWardSerializer
    filter_backends = [
        DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter,
    ]
    filterset_class = MoneyTabWardFilter
    search_fields = "__all__"
    ordering_fields = "__all__"
