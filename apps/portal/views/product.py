from rest_framework import viewsets

from apps.portal.models.product import Product
from apps.portal.serializers import ProductSerializer
from apps.auth0.permissions import RequiredRoles


class ProductsViewSet(viewsets.ModelViewSet):
    """
    Get all products available
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_permissions(self):
        """
        Staff allowed to list method only
        Manager and Admin has full access.
        """
        if self.action in ["list", "retrieve"]:
            permission_classes = [
                RequiredRoles(["admin"])
                | RequiredRoles(["manager"])
                | RequiredRoles(["staff"])
            ]
        else:
            permission_classes = [RequiredRoles(["admin"]) | RequiredRoles(["manager"])]
        return [permission() for permission in permission_classes]
