from rest_framework import viewsets

from ..models.ticket import Ticket
from ..serializers import TicketSerializer


class TicketModelViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
