from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from apps.portal.urls import urlpatterns as portal_urls

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

app_name = "api"
urlpatterns = router.urls
urlpatterns += portal_urls
