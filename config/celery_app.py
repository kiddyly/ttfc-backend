import os
import environ
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")
env = environ.Env()

DEBUG = env.bool("DJANGO_DEBUG")
if DEBUG:
    import ptvsd

    ptvsd.enable_attach(address=("0.0.0.0", 8081))
    print("Attached!")

app = Celery("portal")
app.config_from_object("django.conf:settings", namespace="CELERY")

app.conf.beat_schedule = {
    "clean-lead-generating-at-11PM": {
        "task": "tasks.clean_lead_generating",
        "schedule": crontab(minute=0, hour=16),
    },
    # Temporary disable this task and wait until MT enable again
    # "update-status-partner-lead-every-10-minutes": {
    #     "task": "tasks.update_status_partner_lead",
    #     "schedule": crontab(minute="*/10", hour="1-13"),
    # },
}

app.autodiscover_tasks()
