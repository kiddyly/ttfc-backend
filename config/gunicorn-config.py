loglevel = 'info'
accesslog = '/var/log/gunicorn/access.log'
acceslogformat = "%(h)s %(l)s %(u)s %(t)s %(r)s %(s)s %(b)s %(f)s %(a)s"
errorlog = '/var/log/gunicorn/error.log'
capture_output = True
enable_stdio_inheritance = True
