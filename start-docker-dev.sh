. ./.envs/.dev/env-dev.sh

docker-compose -f dev.yml down

# mkdir -p "$TTFC_TRAEFIK_PATH"
mkdir -p "$TTFC_DJANGO_MEDIA_PATH"
mkdir -p "$TTFC_DJANGO_GUNICORN_PATH"
mkdir -p "$TTFC_POSTGRES_DATA_PATH"
mkdir -p "$TTFC_POSTGRES_DATA_BACKUPS_PATH"

rm -f .env && cp .env.dev .env

docker-compose -f dev.yml up --build -d
# docker-compose exec -T ttfc_django sh -c "python manage.py migrate"